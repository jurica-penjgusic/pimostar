﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FarmWebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                //if (!db.Roles.Any())
                //{
                //    db.Roles.Add(new IdentityRole<int>
                //    {
                //        Name = Role.Counselor.ToString(),
                //        NormalizedName = Role.Counselor.ToString().ToUpper(CultureInfo.InvariantCulture)
                //    });

                //    db.Roles.Add(new IdentityRole<int>
                //    {
                //        Name = Role.Patient.ToString(),
                //        NormalizedName = Role.Patient.ToString().ToUpper(CultureInfo.InvariantCulture)
                //    });

                //    db.SaveChanges();
                //}

                host.Run();
            }

        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
