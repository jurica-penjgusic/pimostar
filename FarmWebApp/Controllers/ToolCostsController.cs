﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.ToolsAndMechanism;

namespace FarmWebApp.Controllers
{
    public class ToolCostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ToolCostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ToolCosts
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ToolCosts.Include(t => t.ToolCostType).Include(t => t.ToolsAndMechanism);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ToolCosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolCost = await _context.ToolCosts
                .Include(t => t.ToolCostType)
                .Include(t => t.ToolsAndMechanism)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolCost == null)
            {
                return NotFound();
            }

            return View(toolCost);
        }

        // GET: ToolCosts/Create
        public IActionResult Create()
        {
            ViewData["CostTypeId"] = new SelectList(_context.ToolCostType, "Id", "Name");
            ViewData["ToolsAndMechanismId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Name");
            return View();
        }

        // POST: ToolCosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ToolsAndMechanismId,Name,CostTime,Amount,Currency,CostTypeId")] ToolCost toolCost)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toolCost);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CostTypeId"] = new SelectList(_context.ToolCostType, "Id", "Id", toolCost.CostTypeId);
            ViewData["ToolsAndMechanismId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolCost.ToolsAndMechanismId);
            return View(toolCost);
        }

        // GET: ToolCosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolCost = await _context.ToolCosts.SingleOrDefaultAsync(m => m.Id == id);
            if (toolCost == null)
            {
                return NotFound();
            }
            ViewData["CostTypeId"] = new SelectList(_context.ToolCostType, "Id", "Id", toolCost.CostTypeId);
            ViewData["ToolsAndMechanismId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolCost.ToolsAndMechanismId);
            return View(toolCost);
        }

        // POST: ToolCosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ToolsAndMechanismId,Name,CostTime,Amount,Currency,CostTypeId")] ToolCost toolCost)
        {
            if (id != toolCost.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toolCost);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToolCostExists(toolCost.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CostTypeId"] = new SelectList(_context.ToolCostType, "Id", "Id", toolCost.CostTypeId);
            ViewData["ToolsAndMechanismId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolCost.ToolsAndMechanismId);
            return View(toolCost);
        }

        // GET: ToolCosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolCost = await _context.ToolCosts
                .Include(t => t.ToolCostType)
                .Include(t => t.ToolsAndMechanism)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolCost == null)
            {
                return NotFound();
            }

            return View(toolCost);
        }

        // POST: ToolCosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toolCost = await _context.ToolCosts.SingleOrDefaultAsync(m => m.Id == id);
            _context.ToolCosts.Remove(toolCost);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToolCostExists(int id)
        {
            return _context.ToolCosts.Any(e => e.Id == id);
        }
    }
}
