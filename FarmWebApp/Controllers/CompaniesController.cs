﻿using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using FarmWebApp.Data;
using FarmWebApp.Extensions;
using FarmWebApp.Models.Company;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly ApplicationDbContext _context;

        public CompaniesController(ApplicationDbContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public FileContentResult ExportToExcel()
        {
            var compamniList = _context.Companies.Include(c => c.CompanyType).OrderBy(x => x.Id).Select(x => new
            {
                Naziv = x.Name,
                Osnovana = x.Created,
                Tip = x.CompanyType.Name,
                Opis = x.Description
            }).ToList();

            string[] columns = {"Naziv", "Osnovana", "Tip", "Opis"};
            var filecontent = ExcelExportHelper.ExportExcel(compamniList, "Firme I Obrti", true, columns);
            return File(filecontent, ExcelExportHelper.ExcelContentType, "Firme I Obrti.xlsx");
        }

        // GET: Companies
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var qry = _context.Companies.Include(c => c.CompanyType).AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
                qry = qry.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);

            var model = await PagingList.CreateAsync(qry, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: Companies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var company = await _context.Companies
                .Include(c => c.CompanyType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
                return NotFound();

            return View(company);
        }

        // GET: Companies/Create
        public IActionResult Create()
        {
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyTypes, "Id", "Name");
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Created,Description,CompanyTypeId")] Company company)
        {
            if (ModelState.IsValid)
            {
                _context.Add(company);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyTypes, "Id", "Id", company.CompanyTypeId);
            return View(company);
        }

        // GET: Companies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var company = await _context.Companies.SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
                return NotFound();
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyTypes, "Id", "Name", company.CompanyTypeId);
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,Name,Created,Description,CompanyTypeId")] Company company)
        {
            if (id != company.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(company);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyExists(company.Id))
                        return NotFound();
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyTypes, "Id", "Id", company.CompanyTypeId);
            return View(company);
        }

        // GET: Companies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var company = await _context.Companies
                .Include(c => c.CompanyType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
                return NotFound();

            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var company = await _context.Companies.SingleOrDefaultAsync(m => m.Id == id);
            _context.Companies.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.Companies.Where(x => x.Name.Contains(search))
                .Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool CompanyExists(int id)
        {
            return _context.Companies.Any(e => e.Id == id);
        }
    }
}