﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  29.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralOperations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class RuralOperationsController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly ApplicationDbContext _context;

        public RuralOperationsController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;

            _appSettings = settings.Value;
        }

        // GET: RuralOperations
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.RuralOperations.Include(r => r.OperationActivityType).Include(r => r.OperationType)
                .Include(r => r.RuralProperty).AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
                query = query.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: RuralOperations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralOperations = await _context.RuralOperations
                .Include(r => r.OperationActivityType)
                .Include(r => r.OperationType)
                .Include(r => r.RuralProperty)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralOperations == null)
                return NotFound();

            return View(ruralOperations);
        }

        // GET: RuralOperations/Create
        public IActionResult Create()
        {
            ViewData["OperationActivityTypeId"] = new SelectList(_context.OperationActivityTypes, "Id", "Name");
            ViewData["OperationTypeId"] = new SelectList(_context.OperationTypes, "Id", "Name");
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Name");
            return View();
        }

        // POST: RuralOperations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind(
                "Id,RuralPropertyId,Name,Description,OperationDateTime,OperationActivityTypeId,OperationTypeId,Currency,MeasurementUnit,PricePerUnit,Cost")]
            RuralOperations ruralOperations)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralOperations);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["OperationActivityTypeId"] = new SelectList(_context.OperationActivityTypes, "Id", "Id",
                ruralOperations.OperationActivityTypeId);
            ViewData["OperationTypeId"] =
                new SelectList(_context.OperationTypes, "Id", "Id", ruralOperations.OperationTypeId);
            ViewData["RuralPropertyId"] =
                new SelectList(_context.RuralProperties, "Id", "Id", ruralOperations.RuralPropertyId);
            return View(ruralOperations);
        }

        // GET: RuralOperations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralOperations = await _context.RuralOperations.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralOperations == null)
                return NotFound();
            ViewData["OperationActivityTypeId"] = new SelectList(_context.OperationActivityTypes, "Id", "Name",
                ruralOperations.OperationActivityTypeId);
            ViewData["OperationTypeId"] =
                new SelectList(_context.OperationTypes, "Id", "Name", ruralOperations.OperationTypeId);
            ViewData["RuralPropertyId"] =
                new SelectList(_context.RuralProperties, "Id", "Name", ruralOperations.RuralPropertyId);
            return View(ruralOperations);
        }

        // POST: RuralOperations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind(
                "Id,RuralPropertyId,Name,Description,OperationDateTime,OperationActivityTypeId,OperationTypeId,Currency,MeasurementUnit,PricePerUnit,Cost")]
            RuralOperations ruralOperations)
        {
            if (id != ruralOperations.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralOperations);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralOperationsExists(ruralOperations.Id))
                        return NotFound();
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["OperationActivityTypeId"] = new SelectList(_context.OperationActivityTypes, "Id", "Id",
                ruralOperations.OperationActivityTypeId);
            ViewData["OperationTypeId"] =
                new SelectList(_context.OperationTypes, "Id", "Id", ruralOperations.OperationTypeId);
            ViewData["RuralPropertyId"] =
                new SelectList(_context.RuralProperties, "Id", "Id", ruralOperations.RuralPropertyId);
            return View(ruralOperations);
        }

        // GET: RuralOperations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralOperations = await _context.RuralOperations
                .Include(r => r.OperationActivityType)
                .Include(r => r.OperationType)
                .Include(r => r.RuralProperty)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralOperations == null)
                return NotFound();

            return View(ruralOperations);
        }

        // POST: RuralOperations/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralOperations = await _context.RuralOperations.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralOperations.Remove(ruralOperations);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.RuralOperations.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool RuralOperationsExists(int id)
        {
            return _context.RuralOperations.Any(e => e.Id == id);
        }
    }
}