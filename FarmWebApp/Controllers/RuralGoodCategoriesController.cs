﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralGoods;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class RuralGoodCategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public RuralGoodCategoriesController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;

            _appSettings = settings.Value;
        }

        // GET: RuralGoodCategories
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.RuralGoodCategories.AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);
            }
             
            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };


            return View(model);
        }

        // GET: RuralGoodCategories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodCategory = await _context.RuralGoodCategories
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodCategory == null)
            {
                return NotFound();
            }

            return View(ruralGoodCategory);
        }

        // GET: RuralGoodCategories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RuralGoodCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] RuralGoodCategory ruralGoodCategory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralGoodCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ruralGoodCategory);
        }

        // GET: RuralGoodCategories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodCategory = await _context.RuralGoodCategories.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodCategory == null)
            {
                return NotFound();
            }
            return View(ruralGoodCategory);
        }

        // POST: RuralGoodCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] RuralGoodCategory ruralGoodCategory)
        {
            if (id != ruralGoodCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralGoodCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralGoodCategoryExists(ruralGoodCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ruralGoodCategory);
        }

        // GET: RuralGoodCategories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodCategory = await _context.RuralGoodCategories
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodCategory == null)
            {
                return NotFound();
            }

            return View(ruralGoodCategory);
        }

        // POST: RuralGoodCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralGoodCategory = await _context.RuralGoodCategories.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralGoodCategories.Remove(ruralGoodCategory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.RuralGoodCategories.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool RuralGoodCategoryExists(int id)
        {
            return _context.RuralGoodCategories.Any(e => e.Id == id);
        }
    }
}
