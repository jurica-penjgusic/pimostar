﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  29.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using FarmWebApp.Data;
using FarmWebApp.Models.Company;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class CompanyTypesController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly ApplicationDbContext _context;

        public CompanyTypesController(ApplicationDbContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        // GET: CompanyTypes
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var qry = _context.CompanyTypes.AsNoTracking().OrderByDescending(p => p.Id);

            if (!string.IsNullOrEmpty(search))
            {
                qry = qry.Where(x => x.Name.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(qry, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: CompanyTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var companyType = await _context.CompanyTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (companyType == null)
                return NotFound();

            return View(companyType);
        }

        // GET: CompanyTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CompanyTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] CompanyType companyType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companyType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(companyType);
        }

        // GET: CompanyTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var companyType = await _context.CompanyTypes.SingleOrDefaultAsync(m => m.Id == id);
            if (companyType == null)
                return NotFound();
            return View(companyType);
        }

        // POST: CompanyTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] CompanyType companyType)
        {
            if (id != companyType.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companyType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyTypeExists(companyType.Id))
                        return NotFound();
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(companyType);
        }

        // GET: CompanyTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var companyType = await _context.CompanyTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (companyType == null)
                return NotFound();

            return View(companyType);
        }

        // POST: CompanyTypes/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companyType = await _context.CompanyTypes.SingleOrDefaultAsync(m => m.Id == id);
            _context.CompanyTypes.Remove(companyType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.CompanyTypes.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool CompanyTypeExists(int id)
        {
            return _context.CompanyTypes.Any(e => e.Id == id);
        }
    }
}