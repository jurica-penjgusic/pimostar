﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Extensions;
using FarmWebApp.Models.ToolsAndMechanism;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class ToolsAndMechanismController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public ToolsAndMechanismController(ApplicationDbContext context, IOptions<AppSettings> settings )
        {
            _context = context;

            _appSettings = settings.Value;

        }

        [HttpGet]
        public FileContentResult ExportToExcel()
        {
            var compamniList = _context.ToolsAndMechanisms.Include(c => c.Company).Include(x => x.ToolCosts).Include(x => x.ToolsPerRuralProperty).OrderBy(x => x.Id).Select(x => new
            {
                Naziv = x.Name,
                Kolicina = x.Amount,
                Vlasnik = x.Company.Name,
                Opis = x.Description,
                Trosak = x.ToolCosts.Sum(y => y.Amount),
            }).ToList();

            string[] columns = { "Naziv", "Kolicina", "Vlasnik", "Opis", "Trosak" };
            var filecontent = ExcelExportHelper.ExportExcel(compamniList, "Alati I Strojevi", true, columns);
            return File(filecontent, ExcelExportHelper.ExcelContentType, "Alati I Strojevi.xlsx");
        }

        // GET: ToolsAndMechanism
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {

            var qry = _context.ToolsAndMechanisms.Include(c => c.Company).AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                qry = qry.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(qry, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: ToolsAndMechanism/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolsAndMechanism = await _context.ToolsAndMechanisms
                .Include(t => t.Company)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolsAndMechanism == null)
            {
                return NotFound();
            }

            return View(toolsAndMechanism);
        }

        // GET: ToolsAndMechanism/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            return View();
        }

        // POST: ToolsAndMechanism/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CompanyId,Name,Description,UnitMeasurement,Amount")] ToolsAndMechanism toolsAndMechanism)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toolsAndMechanism);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", toolsAndMechanism.CompanyId);
            return View(toolsAndMechanism);
        }

        // GET: ToolsAndMechanism/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolsAndMechanism = await _context.ToolsAndMechanisms.SingleOrDefaultAsync(m => m.Id == id);
            if (toolsAndMechanism == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", toolsAndMechanism.CompanyId);
            return View(toolsAndMechanism);
        }

        // POST: ToolsAndMechanism/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CompanyId,Name,Description,UnitMeasurement,Amount")] ToolsAndMechanism toolsAndMechanism)
        {
            if (id != toolsAndMechanism.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toolsAndMechanism);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToolsAndMechanismExists(toolsAndMechanism.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", toolsAndMechanism.CompanyId);
            return View(toolsAndMechanism);
        }

        // GET: ToolsAndMechanism/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolsAndMechanism = await _context.ToolsAndMechanisms
                .Include(t => t.Company)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolsAndMechanism == null)
            {
                return NotFound();
            }

            return View(toolsAndMechanism);
        }

        // POST: ToolsAndMechanism/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toolsAndMechanism = await _context.ToolsAndMechanisms.SingleOrDefaultAsync(m => m.Id == id);
            _context.ToolsAndMechanisms.Remove(toolsAndMechanism);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.Companies.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool ToolsAndMechanismExists(int id)
        {
            return _context.ToolsAndMechanisms.Any(e => e.Id == id);
        }
    }
}
