﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.SalePlaces;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class SalePlaceCatogoriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public SalePlaceCatogoriesController(ApplicationDbContext context, IOptions<AppSettings> setttings)
        {
            _context = context;

            _appSettings = setttings.Value;
        }

        // GET: SalePlaceCatogories
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.SalePlaceCatogories.AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Name.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: SalePlaceCatogories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salePlaceCatogory = await _context.SalePlaceCatogories
                .SingleOrDefaultAsync(m => m.Id == id);
            if (salePlaceCatogory == null)
            {
                return NotFound();
            }

            return View(salePlaceCatogory);
        }

        // GET: SalePlaceCatogories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SalePlaceCatogories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] SalePlaceCatogory salePlaceCatogory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(salePlaceCatogory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(salePlaceCatogory);
        }

        // GET: SalePlaceCatogories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salePlaceCatogory = await _context.SalePlaceCatogories.SingleOrDefaultAsync(m => m.Id == id);
            if (salePlaceCatogory == null)
            {
                return NotFound();
            }
            return View(salePlaceCatogory);
        }

        // POST: SalePlaceCatogories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] SalePlaceCatogory salePlaceCatogory)
        {
            if (id != salePlaceCatogory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(salePlaceCatogory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SalePlaceCatogoryExists(salePlaceCatogory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(salePlaceCatogory);
        }

        // GET: SalePlaceCatogories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salePlaceCatogory = await _context.SalePlaceCatogories
                .SingleOrDefaultAsync(m => m.Id == id);
            if (salePlaceCatogory == null)
            {
                return NotFound();
            }

            return View(salePlaceCatogory);
        }

        // POST: SalePlaceCatogories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var salePlaceCatogory = await _context.SalePlaceCatogories.SingleOrDefaultAsync(m => m.Id == id);
            _context.SalePlaceCatogories.Remove(salePlaceCatogory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.SalePlaceCatogories.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool SalePlaceCatogoryExists(int id)
        {
            return _context.SalePlaceCatogories.Any(e => e.Id == id);
        }
    }
}
