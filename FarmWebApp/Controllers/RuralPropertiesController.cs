﻿using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralProperties;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class RuralPropertiesController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly ApplicationDbContext _context;

        public RuralPropertiesController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;

            _appSettings = settings.Value;
        }

        // GET: RuralProperties
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.RuralProperties.Include(r => r.Company).Include(r => r.RuralPropertyType)
                .AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
                query = query.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: RuralProperties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralProperty = await _context.RuralProperties
                .Include(r => r.Company)
                .Include(r => r.RuralPropertyType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralProperty == null)
                return NotFound();

            return View(ruralProperty);
        }

        // GET: RuralProperties/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            ViewData["RuralPropertyTypeId"] = new SelectList(_context.RuralPropertyTypes, "Id", "Name");
            return View();
        }

        // POST: RuralProperties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind(
                "Id,Name,LocalIdentifier,Longitute,Latitude,Description,Size,SizeMeasurementUnit,CompanyId,RuralPropertyTypeId")]
            RuralProperty ruralProperty)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralProperty);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", ruralProperty.CompanyId);
            ViewData["RuralPropertyTypeId"] = new SelectList(_context.RuralPropertyTypes, "Id", "Id",
                ruralProperty.RuralPropertyTypeId);
            return View(ruralProperty);
        }

        // GET: RuralProperties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralProperty = await _context.RuralProperties.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralProperty == null)
                return NotFound();
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", ruralProperty.CompanyId);
            ViewData["RuralPropertyTypeId"] = new SelectList(_context.RuralPropertyTypes, "Id", "Name",
                ruralProperty.RuralPropertyTypeId);
            return View(ruralProperty);
        }

        // POST: RuralProperties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind(
                "Id,Name,LocalIdentifier,Longitute,Latitude,Description,Size,SizeMeasurementUnit,CompanyId,RuralPropertyTypeId")]
            RuralProperty ruralProperty)
        {
            if (id != ruralProperty.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralProperty);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralPropertyExists(ruralProperty.Id))
                        return NotFound();
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", ruralProperty.CompanyId);
            ViewData["RuralPropertyTypeId"] = new SelectList(_context.RuralPropertyTypes, "Id", "Id",
                ruralProperty.RuralPropertyTypeId);
            return View(ruralProperty);
        }

        // GET: RuralProperties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralProperty = await _context.RuralProperties
                .Include(r => r.Company)
                .Include(r => r.RuralPropertyType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralProperty == null)
                return NotFound();

            return View(ruralProperty);
        }

        // POST: RuralProperties/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralProperty = await _context.RuralProperties.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralProperties.Remove(ruralProperty);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.RuralProperties.Where(x => x.Name.Contains(search))
                .Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool RuralPropertyExists(int id)
        {
            return _context.RuralProperties.Any(e => e.Id == id);
        }
    }
}