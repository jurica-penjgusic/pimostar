﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Extensions;
using FarmWebApp.Models.MlijekoMati;

namespace FarmWebApp.Controllers
{
    public class TroskoviMlijekomatasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TroskoviMlijekomatasController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public FileContentResult ExportToExcel()
        {
            var compamniList = _context.TroskoviMlijekomata.Include(c => c.MlijekoMat).Include(x => x.TipTroska).OrderBy(x => x.Id).Select(x => new
            {
                NazivMlijekoMata = x.MlijekoMat.Naziv,
                DatumPlacanja = x.DatumPlacanja,
                TipTroska = x.TipTroska.Naziv,
                Valuta = x.Valuta,
                Iznos = x.Iznos
            }).ToList();

            string[] columns = { "NazivMlijekoMata", "DatumPlacanja", "TipTroska", "Valuta", "Iznos" };
            var filecontent = ExcelExportHelper.ExportExcel(compamniList, "Troskovi Mlijekomata", true, columns);
            return File(filecontent, ExcelExportHelper.ExcelContentType, "Troskovi Mlijekomata.xlsx");
        }

        // GET: TroskoviMlijekomatas
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.TroskoviMlijekomata.Include(t => t.MlijekoMat).Include(t => t.TipTroska);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: TroskoviMlijekomatas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var troskoviMlijekomata = await _context.TroskoviMlijekomata
                .Include(t => t.MlijekoMat)
                .Include(t => t.TipTroska)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (troskoviMlijekomata == null)
            {
                return NotFound();
            }

            return View(troskoviMlijekomata);
        }

        // GET: TroskoviMlijekomatas/Create
        public IActionResult Create()
        {
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Naziv");
            ViewData["TipTroskaId"] = new SelectList(_context.Trosak, "Id", "Naziv");
            return View();
        }

        // POST: TroskoviMlijekomatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MlijekomatId,Valuta,Iznos,DatumPlacanja,TipTroskaId")] TroskoviMlijekomata troskoviMlijekomata)
        {
            if (ModelState.IsValid)
            {
                _context.Add(troskoviMlijekomata);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Naziv", troskoviMlijekomata.MlijekomatId);
            ViewData["TipTroskaId"] = new SelectList(_context.Trosak, "Id", "Id", troskoviMlijekomata.TipTroskaId);
            return View(troskoviMlijekomata);
        }

        // GET: TroskoviMlijekomatas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var troskoviMlijekomata = await _context.TroskoviMlijekomata.SingleOrDefaultAsync(m => m.Id == id);
            if (troskoviMlijekomata == null)
            {
                return NotFound();
            }
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Naziv", troskoviMlijekomata.MlijekomatId);
            ViewData["TipTroskaId"] = new SelectList(_context.Trosak, "Id", "Naziv", troskoviMlijekomata.TipTroskaId);
            return View(troskoviMlijekomata);
        }

        // POST: TroskoviMlijekomatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MlijekomatId,Valuta,Iznos,DatumPlacanja,TipTroskaId")] TroskoviMlijekomata troskoviMlijekomata)
        {
            if (id != troskoviMlijekomata.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(troskoviMlijekomata);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TroskoviMlijekomataExists(troskoviMlijekomata.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Id", troskoviMlijekomata.MlijekomatId);
            ViewData["TipTroskaId"] = new SelectList(_context.Trosak, "Id", "Id", troskoviMlijekomata.TipTroskaId);
            return View(troskoviMlijekomata);
        }

        // GET: TroskoviMlijekomatas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var troskoviMlijekomata = await _context.TroskoviMlijekomata
                .Include(t => t.MlijekoMat)
                .Include(t => t.TipTroska)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (troskoviMlijekomata == null)
            {
                return NotFound();
            }

            return View(troskoviMlijekomata);
        }

        // POST: TroskoviMlijekomatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var troskoviMlijekomata = await _context.TroskoviMlijekomata.SingleOrDefaultAsync(m => m.Id == id);
            _context.TroskoviMlijekomata.Remove(troskoviMlijekomata);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TroskoviMlijekomataExists(int id)
        {
            return _context.TroskoviMlijekomata.Any(e => e.Id == id);
        }
    }
}
