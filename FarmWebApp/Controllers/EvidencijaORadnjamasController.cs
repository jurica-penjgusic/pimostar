﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Extensions;
using FarmWebApp.Models.Evidencija;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class EvidencijaORadnjamasController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly AppSettings _appSettings;

        public EvidencijaORadnjamasController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;
            _appSettings = settings.Value;
        }

        [HttpGet]
        public FileContentResult ExportToExcel()
        {
            var compamniList = _context.EvidencijaORadnjama.Include(c => c.GospodarskoDobro).OrderBy(x => x.Id).Select(x => new
            {
                Naziv = x.Name,
                GospodarskoDobro = x.GospodarskoDobro.Name,
                DatumIzvrsenja = x.DatumIzvrsenja,
                Aktivnost = x.TipAktivnosti.Naziv,
                Cijena = x.Cijena,
                Valuta = x.Valuta,
            }).ToList();

            string[] columns = { "Naziv", "GospodarskoDobro", "DatumIzvrsenja", "Aktivnost", "Cijena", "Valuta" };
            var filecontent = ExcelExportHelper.ExportExcel(compamniList, "Evidencije o radnjama", true, columns);
            return File(filecontent, ExcelExportHelper.ExcelContentType, "Evidencije o radnjama.xlsx");
        }

        // GET: EvidencijaORadnjamas
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.EvidencijaORadnjama.Include(e => e.GospodarskoDobro).Include(e => e.TipAktivnosti).AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Name.Contains(search)).OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: EvidencijaORadnjamas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evidencijaORadnjama = await _context.EvidencijaORadnjama
                .Include(e => e.GospodarskoDobro)
                .Include(e => e.TipAktivnosti)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (evidencijaORadnjama == null)
            {
                return NotFound();
            }

            return View(evidencijaORadnjama);
        }

        // GET: EvidencijaORadnjamas/Create
        public IActionResult Create()
        {
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Name");
            ViewData["TipAktivnostiId"] = new SelectList(_context.TipAktivnosti, "Id", "Naziv");
            return View();
        }

        // POST: EvidencijaORadnjamas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,GospodarskoDobroId,DatumIzvrsenja,TipAktivnostiId,Valuta,Cijena")] EvidencijaORadnjama evidencijaORadnjama)
        {
            if (ModelState.IsValid)
            {
                _context.Add(evidencijaORadnjama);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Id", evidencijaORadnjama.GospodarskoDobroId);
            ViewData["TipAktivnostiId"] = new SelectList(_context.TipAktivnosti, "Id", "Id", evidencijaORadnjama.TipAktivnostiId);
            return View(evidencijaORadnjama);
        }

        // GET: EvidencijaORadnjamas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evidencijaORadnjama = await _context.EvidencijaORadnjama.SingleOrDefaultAsync(m => m.Id == id);
            if (evidencijaORadnjama == null)
            {
                return NotFound();
            }
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Name", evidencijaORadnjama.GospodarskoDobroId);
            ViewData["TipAktivnostiId"] = new SelectList(_context.TipAktivnosti, "Id", "Naziv", evidencijaORadnjama.TipAktivnostiId);
            return View(evidencijaORadnjama);
        }

        // POST: EvidencijaORadnjamas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,GospodarskoDobroId,DatumIzvrsenja,TipAktivnostiId,Valuta,Cijena")] EvidencijaORadnjama evidencijaORadnjama)
        {
            if (id != evidencijaORadnjama.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(evidencijaORadnjama);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvidencijaORadnjamaExists(evidencijaORadnjama.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Id", evidencijaORadnjama.GospodarskoDobroId);
            ViewData["TipAktivnostiId"] = new SelectList(_context.TipAktivnosti, "Id", "Id", evidencijaORadnjama.TipAktivnostiId);
            return View(evidencijaORadnjama);
        }

        // GET: EvidencijaORadnjamas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evidencijaORadnjama = await _context.EvidencijaORadnjama
                .Include(e => e.GospodarskoDobro)
                .Include(e => e.TipAktivnosti)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (evidencijaORadnjama == null)
            {
                return NotFound();
            }

            return View(evidencijaORadnjama);
        }

        // POST: EvidencijaORadnjamas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evidencijaORadnjama = await _context.EvidencijaORadnjama.SingleOrDefaultAsync(m => m.Id == id);
            _context.EvidencijaORadnjama.Remove(evidencijaORadnjama);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.EvidencijaORadnjama.Where(x => x.Name.Contains(search))
                .Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool EvidencijaORadnjamaExists(int id)
        {
            return _context.EvidencijaORadnjama.Any(e => e.Id == id);
        }
    }
}
