﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Extensions;
using FarmWebApp.Models.MlijekoMati;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class MlijekoMatsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public MlijekoMatsController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;
            _appSettings = settings.Value;
        }

        [HttpGet]
        public FileContentResult ExportToExcel()
        {
            var compamniList = _context.MlijekoMati.Include(c => c.Company).Include(x => x.Troskovi).Include(x => x.Prihod).OrderBy(x => x.Id).Select(x => new
            {
                Naziv = x.Naziv,
                LokalniNaziv = x.LokalniNaziv,
                Vlasnik = x.Company.Name,
                Lokacija = x.Lokacija,
                Trosak = x.Troskovi.Sum(y => y.Iznos),
                Prihod = x.Prihod.Sum(y => y.ProdanaKolicina)
            }).ToList();

            string[] columns = { "Naziv", "Vlasnik", "Lokacija", "Trosak", "Prihod" };
            var filecontent = ExcelExportHelper.ExportExcel(compamniList, "Mlijekomati", true, columns);
            return File(filecontent, ExcelExportHelper.ExcelContentType, "Mlijekomati.xlsx");
        }

        // GET: MlijekoMats
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.MlijekoMati.Include(m => m.Company).AsNoTracking().OrderByDescending(x => x.Id);
            if (!string.IsNullOrEmpty(search))
                query = query.Where(x => x.Naziv.Contains(search) || x.LokalniNaziv.Contains(search))
                    .OrderByDescending(x => x.Id);

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: MlijekoMats/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mlijekoMat = await _context.MlijekoMati
                .Include(m => m.Company)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (mlijekoMat == null)
            {
                return NotFound();
            }

            return View(mlijekoMat);
        }

        // GET: MlijekoMats/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            return View();
        }

        // POST: MlijekoMats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Lokacija,LokalniNaziv,CompanyId")] MlijekoMat mlijekoMat)
        {
            if (ModelState.IsValid)
            {
                _context.Add(mlijekoMat);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", mlijekoMat.CompanyId);
            return View(mlijekoMat);
        }

        // GET: MlijekoMats/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mlijekoMat = await _context.MlijekoMati.SingleOrDefaultAsync(m => m.Id == id);
            if (mlijekoMat == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", mlijekoMat.CompanyId);
            return View(mlijekoMat);
        }

        // POST: MlijekoMats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Lokacija,LokalniNaziv,CompanyId")] MlijekoMat mlijekoMat)
        {
            if (id != mlijekoMat.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(mlijekoMat);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MlijekoMatExists(mlijekoMat.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", mlijekoMat.CompanyId);
            return View(mlijekoMat);
        }

        // GET: MlijekoMats/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mlijekoMat = await _context.MlijekoMati
                .Include(m => m.Company)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (mlijekoMat == null)
            {
                return NotFound();
            }

            return View(mlijekoMat);
        }

        // POST: MlijekoMats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mlijekoMat = await _context.MlijekoMati.SingleOrDefaultAsync(m => m.Id == id);
            _context.MlijekoMati.Remove(mlijekoMat);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.MlijekoMati.Where(x => x.Naziv.Contains(search))
                .Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool MlijekoMatExists(int id)
        {
            return _context.MlijekoMati.Any(e => e.Id == id);
        }
    }
}
