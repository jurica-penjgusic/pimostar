﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.ToolsAndMechanism;

namespace FarmWebApp.Controllers
{
    public class ToolsOutsourcedsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ToolsOutsourcedsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ToolsOutsourceds
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ToolsOutsources.Include(t => t.Company).Include(t => t.ToolsAndMechanism);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ToolsOutsourceds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolsOutsourced = await _context.ToolsOutsources
                .Include(t => t.Company)
                .Include(t => t.ToolsAndMechanism)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolsOutsourced == null)
            {
                return NotFound();
            }

            return View(toolsOutsourced);
        }

        // GET: ToolsOutsourceds/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Name");
            return View();
        }

        // POST: ToolsOutsourceds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ToolId,CompanyId,From,To,Description,Currency,Price")] ToolsOutsourced toolsOutsourced)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toolsOutsourced);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", toolsOutsourced.CompanyId);
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolsOutsourced.ToolId);
            return View(toolsOutsourced);
        }

        // GET: ToolsOutsourceds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolsOutsourced = await _context.ToolsOutsources.SingleOrDefaultAsync(m => m.Id == id);
            if (toolsOutsourced == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", toolsOutsourced.CompanyId);
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Name", toolsOutsourced.ToolId);
            return View(toolsOutsourced);
        }

        // POST: ToolsOutsourceds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ToolId,CompanyId,From,To,Description,Currency,Price")] ToolsOutsourced toolsOutsourced)
        {
            if (id != toolsOutsourced.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toolsOutsourced);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToolsOutsourcedExists(toolsOutsourced.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", toolsOutsourced.CompanyId);
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolsOutsourced.ToolId);
            return View(toolsOutsourced);
        }

        // GET: ToolsOutsourceds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolsOutsourced = await _context.ToolsOutsources
                .Include(t => t.Company)
                .Include(t => t.ToolsAndMechanism)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolsOutsourced == null)
            {
                return NotFound();
            }

            return View(toolsOutsourced);
        }

        // POST: ToolsOutsourceds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toolsOutsourced = await _context.ToolsOutsources.SingleOrDefaultAsync(m => m.Id == id);
            _context.ToolsOutsources.Remove(toolsOutsourced);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToolsOutsourcedExists(int id)
        {
            return _context.ToolsOutsources.Any(e => e.Id == id);
        }
    }
}
