﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.MlijekoMati;

namespace FarmWebApp.Controllers
{
    public class PrihodiMlijekomatasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PrihodiMlijekomatasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PrihodiMlijekomatas
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PrihodiMlijekomata.Include(p => p.MlijekoMat).Include(p => p.TipRadnje);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: PrihodiMlijekomatas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prihodiMlijekomata = await _context.PrihodiMlijekomata
                .Include(p => p.MlijekoMat)
                .Include(p => p.TipRadnje)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (prihodiMlijekomata == null)
            {
                return NotFound();
            }

            return View(prihodiMlijekomata);
        }

        // GET: PrihodiMlijekomatas/Create
        public IActionResult Create()
        {
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Naziv");
            ViewData["TipRadnjeId"] = new SelectList(_context.TipRadnje, "Id", "Naziv");
            return View();
        }

        // POST: PrihodiMlijekomatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MlijekomatId,ProdanaKolicina,VracenaKolicina,TipRadnjeId")] PrihodiMlijekomata prihodiMlijekomata)
        {
            if (ModelState.IsValid)
            {
                _context.Add(prihodiMlijekomata);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Naziv", prihodiMlijekomata.MlijekomatId);
            ViewData["TipRadnjeId"] = new SelectList(_context.TipRadnje, "Id", "Naziv", prihodiMlijekomata.TipRadnjeId);
            return View(prihodiMlijekomata);
        }

        // GET: PrihodiMlijekomatas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prihodiMlijekomata = await _context.PrihodiMlijekomata.SingleOrDefaultAsync(m => m.Id == id);
            if (prihodiMlijekomata == null)
            {
                return NotFound();
            }
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Naziv", prihodiMlijekomata.MlijekomatId);
            ViewData["TipRadnjeId"] = new SelectList(_context.TipRadnje, "Id", "Naziv", prihodiMlijekomata.TipRadnjeId);
            return View(prihodiMlijekomata);
        }

        // POST: PrihodiMlijekomatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MlijekomatId,ProdanaKolicina,VracenaKolicina,TipRadnjeId")] PrihodiMlijekomata prihodiMlijekomata)
        {
            if (id != prihodiMlijekomata.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(prihodiMlijekomata);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PrihodiMlijekomataExists(prihodiMlijekomata.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MlijekomatId"] = new SelectList(_context.MlijekoMati, "Id", "Id", prihodiMlijekomata.MlijekomatId);
            ViewData["TipRadnjeId"] = new SelectList(_context.TipRadnje, "Id", "Id", prihodiMlijekomata.TipRadnjeId);
            return View(prihodiMlijekomata);
        }

        // GET: PrihodiMlijekomatas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var prihodiMlijekomata = await _context.PrihodiMlijekomata
                .Include(p => p.MlijekoMat)
                .Include(p => p.TipRadnje)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (prihodiMlijekomata == null)
            {
                return NotFound();
            }

            return View(prihodiMlijekomata);
        }

        // POST: PrihodiMlijekomatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var prihodiMlijekomata = await _context.PrihodiMlijekomata.SingleOrDefaultAsync(m => m.Id == id);
            _context.PrihodiMlijekomata.Remove(prihodiMlijekomata);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PrihodiMlijekomataExists(int id)
        {
            return _context.PrihodiMlijekomata.Any(e => e.Id == id);
        }
    }
}
