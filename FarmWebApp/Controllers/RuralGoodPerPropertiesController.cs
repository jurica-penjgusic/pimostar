﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralGoods;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class RuralGoodPerPropertiesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public RuralGoodPerPropertiesController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;

            _appSettings = settings.Value;
        }

        // GET: RuralGoodPerProperties
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.RuralGoodPerProperties.Include(r => r.RuralGood).Include(r => r.RuralProperty)
                .AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.RuralGood.Name.Contains(search) || x.RuralProperty.Name.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: RuralGoodPerProperties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodPerProperty = await _context.RuralGoodPerProperties
                .Include(r => r.RuralGood)
                .Include(r => r.RuralProperty)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodPerProperty == null)
            {
                return NotFound();
            }

            return View(ruralGoodPerProperty);
        }

        // GET: RuralGoodPerProperties/Create
        public IActionResult Create()
        {
            ViewData["RuralGoodId"] = new SelectList(_context.RuralGoods, "Id", "Name");
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Name");
            return View();
        }

        // POST: RuralGoodPerProperties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RuralGoodId,RuralPropertyId,TimeOfProductPerLand,MeasurementUnit,Amount")] RuralGoodPerProperty ruralGoodPerProperty)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralGoodPerProperty);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RuralGoodId"] = new SelectList(_context.RuralGoods, "Id", "Id", ruralGoodPerProperty.RuralGoodId);
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Id", ruralGoodPerProperty.RuralPropertyId);
            return View(ruralGoodPerProperty);
        }

        // GET: RuralGoodPerProperties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodPerProperty = await _context.RuralGoodPerProperties.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodPerProperty == null)
            {
                return NotFound();
            }
            ViewData["RuralGoodId"] = new SelectList(_context.RuralGoods, "Id", "Name", ruralGoodPerProperty.RuralGoodId);
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Name", ruralGoodPerProperty.RuralPropertyId);
            return View(ruralGoodPerProperty);
        }

        // POST: RuralGoodPerProperties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,RuralGoodId,RuralPropertyId,TimeOfProductPerLand,MeasurementUnit,Amount")] RuralGoodPerProperty ruralGoodPerProperty)
        {
            if (id != ruralGoodPerProperty.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralGoodPerProperty);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralGoodPerPropertyExists(ruralGoodPerProperty.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RuralGoodId"] = new SelectList(_context.RuralGoods, "Id", "Id", ruralGoodPerProperty.RuralGoodId);
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Id", ruralGoodPerProperty.RuralPropertyId);
            return View(ruralGoodPerProperty);
        }

        // GET: RuralGoodPerProperties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodPerProperty = await _context.RuralGoodPerProperties
                .Include(r => r.RuralGood)
                .Include(r => r.RuralProperty)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodPerProperty == null)
            {
                return NotFound();
            }

            return View(ruralGoodPerProperty);
        }

        // POST: RuralGoodPerProperties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralGoodPerProperty = await _context.RuralGoodPerProperties.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralGoodPerProperties.Remove(ruralGoodPerProperty);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.RuralGoodPerProperties.Include(x => x.RuralGood).Where(x => x.RuralGood.Name.Contains(search))
                .Take(_appSettings.AutoCompleteCount).ToListAsync();

            var result2 = result.Select(x => x.RuralGood);

            return Json(result2);
        }

        private bool RuralGoodPerPropertyExists(int id)
        {
            return _context.RuralGoodPerProperties.Any(e => e.Id == id);
        }
    }
}
