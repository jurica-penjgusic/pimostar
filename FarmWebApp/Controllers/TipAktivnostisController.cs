﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.Evidencija;

namespace FarmWebApp.Controllers
{
    public class TipAktivnostisController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipAktivnostisController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TipAktivnostis
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipAktivnosti.ToListAsync());
        }

        // GET: TipAktivnostis/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipAktivnosti = await _context.TipAktivnosti
                .SingleOrDefaultAsync(m => m.Id == id);
            if (tipAktivnosti == null)
            {
                return NotFound();
            }

            return View(tipAktivnosti);
        }

        // GET: TipAktivnostis/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipAktivnostis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Opis")] TipAktivnosti tipAktivnosti)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipAktivnosti);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipAktivnosti);
        }

        // GET: TipAktivnostis/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipAktivnosti = await _context.TipAktivnosti.SingleOrDefaultAsync(m => m.Id == id);
            if (tipAktivnosti == null)
            {
                return NotFound();
            }
            return View(tipAktivnosti);
        }

        // POST: TipAktivnostis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Opis")] TipAktivnosti tipAktivnosti)
        {
            if (id != tipAktivnosti.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipAktivnosti);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipAktivnostiExists(tipAktivnosti.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipAktivnosti);
        }

        // GET: TipAktivnostis/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipAktivnosti = await _context.TipAktivnosti
                .SingleOrDefaultAsync(m => m.Id == id);
            if (tipAktivnosti == null)
            {
                return NotFound();
            }

            return View(tipAktivnosti);
        }

        // POST: TipAktivnostis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipAktivnosti = await _context.TipAktivnosti.SingleOrDefaultAsync(m => m.Id == id);
            _context.TipAktivnosti.Remove(tipAktivnosti);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipAktivnostiExists(int id)
        {
            return _context.TipAktivnosti.Any(e => e.Id == id);
        }
    }
}
