﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.ToolsAndMechanism;

namespace FarmWebApp.Controllers
{
    public class RuralPropertyActivitiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RuralPropertyActivitiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RuralPropertyActivities
        public async Task<IActionResult> Index()
        {
            return View(await _context.RuralPropertyActivities.ToListAsync());
        }

        // GET: RuralPropertyActivities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralPropertyActivity = await _context.RuralPropertyActivities
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralPropertyActivity == null)
            {
                return NotFound();
            }

            return View(ruralPropertyActivity);
        }

        // GET: RuralPropertyActivities/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RuralPropertyActivities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] RuralPropertyActivity ruralPropertyActivity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralPropertyActivity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ruralPropertyActivity);
        }

        // GET: RuralPropertyActivities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralPropertyActivity = await _context.RuralPropertyActivities.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralPropertyActivity == null)
            {
                return NotFound();
            }
            return View(ruralPropertyActivity);
        }

        // POST: RuralPropertyActivities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] RuralPropertyActivity ruralPropertyActivity)
        {
            if (id != ruralPropertyActivity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralPropertyActivity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralPropertyActivityExists(ruralPropertyActivity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ruralPropertyActivity);
        }

        // GET: RuralPropertyActivities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralPropertyActivity = await _context.RuralPropertyActivities
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralPropertyActivity == null)
            {
                return NotFound();
            }

            return View(ruralPropertyActivity);
        }

        // POST: RuralPropertyActivities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralPropertyActivity = await _context.RuralPropertyActivities.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralPropertyActivities.Remove(ruralPropertyActivity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RuralPropertyActivityExists(int id)
        {
            return _context.RuralPropertyActivities.Any(e => e.Id == id);
        }
    }
}
