﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.ToolsAndMechanism;

namespace FarmWebApp.Controllers
{
    public class ToolCostTypesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ToolCostTypesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ToolCostTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.ToolCostType.ToListAsync());
        }

        // GET: ToolCostTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolCostType = await _context.ToolCostType
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolCostType == null)
            {
                return NotFound();
            }

            return View(toolCostType);
        }

        // GET: ToolCostTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToolCostTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] ToolCostType toolCostType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toolCostType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(toolCostType);
        }

        // GET: ToolCostTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolCostType = await _context.ToolCostType.SingleOrDefaultAsync(m => m.Id == id);
            if (toolCostType == null)
            {
                return NotFound();
            }
            return View(toolCostType);
        }

        // POST: ToolCostTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] ToolCostType toolCostType)
        {
            if (id != toolCostType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toolCostType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToolCostTypeExists(toolCostType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toolCostType);
        }

        // GET: ToolCostTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolCostType = await _context.ToolCostType
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolCostType == null)
            {
                return NotFound();
            }

            return View(toolCostType);
        }

        // POST: ToolCostTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toolCostType = await _context.ToolCostType.SingleOrDefaultAsync(m => m.Id == id);
            _context.ToolCostType.Remove(toolCostType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToolCostTypeExists(int id)
        {
            return _context.ToolCostType.Any(e => e.Id == id);
        }
    }
}
