﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.MlijekoMati;

namespace FarmWebApp.Controllers
{
    public class TipRadnjesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipRadnjesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TipRadnjes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipRadnje.ToListAsync());
        }

        // GET: TipRadnjes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipRadnje = await _context.TipRadnje
                .SingleOrDefaultAsync(m => m.Id == id);
            if (tipRadnje == null)
            {
                return NotFound();
            }

            return View(tipRadnje);
        }

        // GET: TipRadnjes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipRadnjes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Opis")] TipRadnje tipRadnje)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipRadnje);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipRadnje);
        }

        // GET: TipRadnjes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipRadnje = await _context.TipRadnje.SingleOrDefaultAsync(m => m.Id == id);
            if (tipRadnje == null)
            {
                return NotFound();
            }
            return View(tipRadnje);
        }

        // POST: TipRadnjes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Opis")] TipRadnje tipRadnje)
        {
            if (id != tipRadnje.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipRadnje);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipRadnjeExists(tipRadnje.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipRadnje);
        }

        // GET: TipRadnjes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipRadnje = await _context.TipRadnje
                .SingleOrDefaultAsync(m => m.Id == id);
            if (tipRadnje == null)
            {
                return NotFound();
            }

            return View(tipRadnje);
        }

        // POST: TipRadnjes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipRadnje = await _context.TipRadnje.SingleOrDefaultAsync(m => m.Id == id);
            _context.TipRadnje.Remove(tipRadnje);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipRadnjeExists(int id)
        {
            return _context.TipRadnje.Any(e => e.Id == id);
        }
    }
}
