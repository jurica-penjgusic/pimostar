﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.MlijekoMati;

namespace FarmWebApp.Controllers
{
    public class TrosaksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TrosaksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Trosaks
        public async Task<IActionResult> Index()
        {
            return View(await _context.Trosak.ToListAsync());
        }

        // GET: Trosaks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trosak = await _context.Trosak
                .SingleOrDefaultAsync(m => m.Id == id);
            if (trosak == null)
            {
                return NotFound();
            }

            return View(trosak);
        }

        // GET: Trosaks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Trosaks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Opis")] Trosak trosak)
        {
            if (ModelState.IsValid)
            {
                _context.Add(trosak);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trosak);
        }

        // GET: Trosaks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trosak = await _context.Trosak.SingleOrDefaultAsync(m => m.Id == id);
            if (trosak == null)
            {
                return NotFound();
            }
            return View(trosak);
        }

        // POST: Trosaks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Opis")] Trosak trosak)
        {
            if (id != trosak.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trosak);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrosakExists(trosak.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(trosak);
        }

        // GET: Trosaks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trosak = await _context.Trosak
                .SingleOrDefaultAsync(m => m.Id == id);
            if (trosak == null)
            {
                return NotFound();
            }

            return View(trosak);
        }

        // POST: Trosaks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trosak = await _context.Trosak.SingleOrDefaultAsync(m => m.Id == id);
            _context.Trosak.Remove(trosak);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrosakExists(int id)
        {
            return _context.Trosak.Any(e => e.Id == id);
        }
    }
}
