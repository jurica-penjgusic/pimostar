﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.Evidencija;

namespace FarmWebApp.Controllers
{
    public class TipPracenjasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipPracenjasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TipPracenjas
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipPracenja.ToListAsync());
        }

        // GET: TipPracenjas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipPracenja = await _context.TipPracenja
                .SingleOrDefaultAsync(m => m.Id == id);
            if (tipPracenja == null)
            {
                return NotFound();
            }

            return View(tipPracenja);
        }

        // GET: TipPracenjas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipPracenjas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Opis")] TipPracenja tipPracenja)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipPracenja);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipPracenja);
        }

        // GET: TipPracenjas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipPracenja = await _context.TipPracenja.SingleOrDefaultAsync(m => m.Id == id);
            if (tipPracenja == null)
            {
                return NotFound();
            }
            return View(tipPracenja);
        }

        // POST: TipPracenjas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Opis")] TipPracenja tipPracenja)
        {
            if (id != tipPracenja.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipPracenja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipPracenjaExists(tipPracenja.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipPracenja);
        }

        // GET: TipPracenjas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipPracenja = await _context.TipPracenja
                .SingleOrDefaultAsync(m => m.Id == id);
            if (tipPracenja == null)
            {
                return NotFound();
            }

            return View(tipPracenja);
        }

        // POST: TipPracenjas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipPracenja = await _context.TipPracenja.SingleOrDefaultAsync(m => m.Id == id);
            _context.TipPracenja.Remove(tipPracenja);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipPracenjaExists(int id)
        {
            return _context.TipPracenja.Any(e => e.Id == id);
        }
    }
}
