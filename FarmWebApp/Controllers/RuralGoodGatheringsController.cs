﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralGoodGathering;

namespace FarmWebApp.Controllers
{
    public class RuralGoodGatheringsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RuralGoodGatheringsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RuralGoodGatherings
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.RuralGoodGatherings.Include(r => r.GatheringCosts).Include(r => r.RuralGoodPerProperty).Include(r => r.Season);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: RuralGoodGatherings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodGathering = await _context.RuralGoodGatherings
                .Include(r => r.GatheringCosts)
                .Include(r => r.RuralGoodPerProperty)
                .Include(r => r.Season)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodGathering == null)
            {
                return NotFound();
            }

            return View(ruralGoodGathering);
        }

        // GET: RuralGoodGatherings/Create
        public IActionResult Create()
        {
            ViewData["GatheringCostId"] = new SelectList(_context.GatheringCosts, "Id", "Name");
            ViewData["RuralGoodPerPropertyId"] = new SelectList(_context.RuralGoodPerProperties, "Id", "Name");
            ViewData["SeasonId"] = new SelectList(_context.Seasons, "Id", "Name");
            return View();
        }

        // POST: RuralGoodGatherings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RuralGoodPerPropertyId,MeasurementUnit,Amount,GatheringTime,GatheringCostId,SeasonId")] RuralGoodGathering ruralGoodGathering)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralGoodGathering);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GatheringCostId"] = new SelectList(_context.GatheringCosts, "Id", "Id", ruralGoodGathering.GatheringCostId);
            ViewData["RuralGoodPerPropertyId"] = new SelectList(_context.RuralGoodPerProperties, "Id", "Id", ruralGoodGathering.RuralGoodPerPropertyId);
            ViewData["SeasonId"] = new SelectList(_context.Seasons, "Id", "Name", ruralGoodGathering.SeasonId);
            return View(ruralGoodGathering);
        }

        // GET: RuralGoodGatherings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodGathering = await _context.RuralGoodGatherings.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodGathering == null)
            {
                return NotFound();
            }
            ViewData["GatheringCostId"] = new SelectList(_context.GatheringCosts, "Id", "Name", ruralGoodGathering.GatheringCostId);
            ViewData["RuralGoodPerPropertyId"] = new SelectList(_context.RuralGoodPerProperties, "Id", "Name", ruralGoodGathering.RuralGoodPerPropertyId);
            ViewData["SeasonId"] = new SelectList(_context.Seasons, "Id", "Name", ruralGoodGathering.SeasonId);
            return View(ruralGoodGathering);
        }

        // POST: RuralGoodGatherings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,RuralGoodPerPropertyId,MeasurementUnit,Amount,GatheringTime,GatheringCostId,SeasonId")] RuralGoodGathering ruralGoodGathering)
        {
            if (id != ruralGoodGathering.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralGoodGathering);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralGoodGatheringExists(ruralGoodGathering.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GatheringCostId"] = new SelectList(_context.GatheringCosts, "Id", "Id", ruralGoodGathering.GatheringCostId);
            ViewData["RuralGoodPerPropertyId"] = new SelectList(_context.RuralGoodPerProperties, "Id", "Id", ruralGoodGathering.RuralGoodPerPropertyId);
            ViewData["SeasonId"] = new SelectList(_context.Seasons, "Id", "Name", ruralGoodGathering.SeasonId);
            return View(ruralGoodGathering);
        }

        // GET: RuralGoodGatherings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralGoodGathering = await _context.RuralGoodGatherings
                .Include(r => r.GatheringCosts)
                .Include(r => r.RuralGoodPerProperty)
                .Include(r => r.Season)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGoodGathering == null)
            {
                return NotFound();
            }

            return View(ruralGoodGathering);
        }

        // POST: RuralGoodGatherings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralGoodGathering = await _context.RuralGoodGatherings.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralGoodGatherings.Remove(ruralGoodGathering);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RuralGoodGatheringExists(int id)
        {
            return _context.RuralGoodGatherings.Any(e => e.Id == id);
        }
    }
}
