﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.Evidencija;

namespace FarmWebApp.Controllers
{
    public class EvidencijasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EvidencijasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Evidencijas
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Evidencija.Include(e => e.GosparskoDobro).Include(e => e.TipPracenja);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Evidencijas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evidencija = await _context.Evidencija
                .Include(e => e.GosparskoDobro)
                .Include(e => e.TipPracenja)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (evidencija == null)
            {
                return NotFound();
            }

            return View(evidencija);
        }

        // GET: Evidencijas/Create
        public IActionResult Create()
        {
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Name");
            ViewData["TipPracenjaId"] = new SelectList(_context.TipPracenja, "Id", "Naziv");
            return View();
        }

        // POST: Evidencijas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NazivEvidencije,GospodarskoDobroId,TipPracenjaId,MjernaJedinica,PrethodnoStanje,TrenutnoStanje,DatumZapisa")] Evidencija evidencija)
        {
            if (ModelState.IsValid)
            {
                _context.Add(evidencija);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Name", evidencija.GospodarskoDobroId);
            ViewData["TipPracenjaId"] = new SelectList(_context.TipPracenja, "Id", "Naziv", evidencija.TipPracenjaId);
            return View(evidencija);
        }

        // GET: Evidencijas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evidencija = await _context.Evidencija.SingleOrDefaultAsync(m => m.Id == id);
            if (evidencija == null)
            {
                return NotFound();
            }
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Name", evidencija.GospodarskoDobroId);
            ViewData["TipPracenjaId"] = new SelectList(_context.TipPracenja, "Id", "Naziv", evidencija.TipPracenjaId);
            return View(evidencija);
        }

        // POST: Evidencijas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NazivEvidencije,GospodarskoDobroId,TipPracenjaId,MjernaJedinica,PrethodnoStanje,TrenutnoStanje,DatumZapisa")] Evidencija evidencija)
        {
            if (id != evidencija.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(evidencija);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvidencijaExists(evidencija.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GospodarskoDobroId"] = new SelectList(_context.RuralGoods, "Id", "Id", evidencija.GospodarskoDobroId);
            ViewData["TipPracenjaId"] = new SelectList(_context.TipPracenja, "Id", "Id", evidencija.TipPracenjaId);
            return View(evidencija);
        }

        // GET: Evidencijas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evidencija = await _context.Evidencija
                .Include(e => e.GosparskoDobro)
                .Include(e => e.TipPracenja)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (evidencija == null)
            {
                return NotFound();
            }

            return View(evidencija);
        }

        // POST: Evidencijas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evidencija = await _context.Evidencija.SingleOrDefaultAsync(m => m.Id == id);
            _context.Evidencija.Remove(evidencija);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EvidencijaExists(int id)
        {
            return _context.Evidencija.Any(e => e.Id == id);
        }
    }
}
