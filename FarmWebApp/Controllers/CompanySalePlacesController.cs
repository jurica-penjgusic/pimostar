﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Extensions;
using FarmWebApp.Models.SalePlaces;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class CompanySalePlacesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public CompanySalePlacesController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;
            _appSettings = settings.Value;
        }

        [HttpGet]
        public FileContentResult ExportToExcel()
        {
            var list = _context.CompanySalePlaces.Include(c => c.Company).OrderBy(x => x.Id).Select(x => new
            {
                Naziv = x.SalePlaceName,
                Vlasnik = x.Company.Name,
                Kategorija = x.SalePlaceCatogory.Name,

            }).ToList();

            string[] columns = { "Naziv", "Vlasnik", "Kategorija" };
            var filecontent = ExcelExportHelper.ExportExcel(list, "Prodajna mjesta", true, columns);
            return File(filecontent, ExcelExportHelper.ExcelContentType, "Prodajna mjesta.xlsx");
        }

        // GET: CompanySalePlaces
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.CompanySalePlaces.Include(c => c.Company)
                .Include(c => c.SalePlaceCatogory).AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.SalePlaceName.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: CompanySalePlaces/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companySalePlace = await _context.CompanySalePlaces
                .Include(c => c.Company)
                .Include(c => c.SalePlaceCatogory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (companySalePlace == null)
            {
                return NotFound();
            }

            return View(companySalePlace);
        }

        // GET: CompanySalePlaces/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            ViewData["SalePlaceCategoryId"] = new SelectList(_context.SalePlaceCatogories, "Id", "Name");
            return View();
        }

        // POST: CompanySalePlaces/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CompanyId,SalePlaceName,Longitude,Latitude,SalePlaceCategoryId")] CompanySalePlace companySalePlace)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companySalePlace);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", companySalePlace.CompanyId);
            ViewData["SalePlaceCategoryId"] = new SelectList(_context.SalePlaceCatogories, "Id", "Id", companySalePlace.SalePlaceCategoryId);
            return View(companySalePlace);
        }

        // GET: CompanySalePlaces/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companySalePlace = await _context.CompanySalePlaces.SingleOrDefaultAsync(m => m.Id == id);
            if (companySalePlace == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", companySalePlace.CompanyId);
            ViewData["SalePlaceCategoryId"] = new SelectList(_context.SalePlaceCatogories, "Id", "Name", companySalePlace.SalePlaceCategoryId);
            return View(companySalePlace);
        }

        // POST: CompanySalePlaces/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CompanyId,SalePlaceName,Longitude,Latitude,SalePlaceCategoryId")] CompanySalePlace companySalePlace)
        {
            if (id != companySalePlace.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companySalePlace);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanySalePlaceExists(companySalePlace.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", companySalePlace.CompanyId);
            ViewData["SalePlaceCategoryId"] = new SelectList(_context.SalePlaceCatogories, "Id", "Id", companySalePlace.SalePlaceCategoryId);
            return View(companySalePlace);
        }

        // GET: CompanySalePlaces/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companySalePlace = await _context.CompanySalePlaces
                .Include(c => c.Company)
                .Include(c => c.SalePlaceCatogory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (companySalePlace == null)
            {
                return NotFound();
            }

            return View(companySalePlace);
        }

        // POST: CompanySalePlaces/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companySalePlace = await _context.CompanySalePlaces.SingleOrDefaultAsync(m => m.Id == id);
            _context.CompanySalePlaces.Remove(companySalePlace);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.CompanySalePlaces.Where(x => x.SalePlaceName.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool CompanySalePlaceExists(int id)
        {
            return _context.CompanySalePlaces.Any(e => e.Id == id);
        }
    }
}
