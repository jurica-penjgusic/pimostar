﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralGoodGathering;

namespace FarmWebApp.Controllers
{
    public class GatheringCostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public GatheringCostsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: GatheringCosts
        public async Task<IActionResult> Index()
        {
            return View(await _context.GatheringCosts.ToListAsync());
        }

        // GET: GatheringCosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gatheringCosts = await _context.GatheringCosts
                .SingleOrDefaultAsync(m => m.Id == id);
            if (gatheringCosts == null)
            {
                return NotFound();
            }

            return View(gatheringCosts);
        }

        // GET: GatheringCosts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: GatheringCosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] GatheringCosts gatheringCosts)
        {
            if (ModelState.IsValid)
            {
                _context.Add(gatheringCosts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(gatheringCosts);
        }

        // GET: GatheringCosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gatheringCosts = await _context.GatheringCosts.SingleOrDefaultAsync(m => m.Id == id);
            if (gatheringCosts == null)
            {
                return NotFound();
            }
            return View(gatheringCosts);
        }

        // POST: GatheringCosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] GatheringCosts gatheringCosts)
        {
            if (id != gatheringCosts.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gatheringCosts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GatheringCostsExists(gatheringCosts.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(gatheringCosts);
        }

        // GET: GatheringCosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gatheringCosts = await _context.GatheringCosts
                .SingleOrDefaultAsync(m => m.Id == id);
            if (gatheringCosts == null)
            {
                return NotFound();
            }

            return View(gatheringCosts);
        }

        // POST: GatheringCosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var gatheringCosts = await _context.GatheringCosts.SingleOrDefaultAsync(m => m.Id == id);
            _context.GatheringCosts.Remove(gatheringCosts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GatheringCostsExists(int id)
        {
            return _context.GatheringCosts.Any(e => e.Id == id);
        }
    }
}
