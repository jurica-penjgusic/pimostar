﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralProperties;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class RuralPropertyTypesController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly AppSettings _appSettings;

        public RuralPropertyTypesController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;

            _appSettings = settings.Value;
        }

        // GET: RuralPropertyTypes
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.RuralPropertyTypes.AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: RuralPropertyTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralPropertyType = await _context.RuralPropertyTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralPropertyType == null)
            {
                return NotFound();
            }

            return View(ruralPropertyType);
        }

        // GET: RuralPropertyTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RuralPropertyTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] RuralPropertyType ruralPropertyType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralPropertyType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ruralPropertyType);
        }

        // GET: RuralPropertyTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralPropertyType = await _context.RuralPropertyTypes.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralPropertyType == null)
            {
                return NotFound();
            }
            return View(ruralPropertyType);
        }

        // POST: RuralPropertyTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] RuralPropertyType ruralPropertyType)
        {
            if (id != ruralPropertyType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralPropertyType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralPropertyTypeExists(ruralPropertyType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ruralPropertyType);
        }

        // GET: RuralPropertyTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruralPropertyType = await _context.RuralPropertyTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralPropertyType == null)
            {
                return NotFound();
            }

            return View(ruralPropertyType);
        }

        // POST: RuralPropertyTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralPropertyType = await _context.RuralPropertyTypes.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralPropertyTypes.Remove(ruralPropertyType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.RuralPropertyTypes.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool RuralPropertyTypeExists(int id)
        {
            return _context.RuralPropertyTypes.Any(e => e.Id == id);
        }
    }
}
