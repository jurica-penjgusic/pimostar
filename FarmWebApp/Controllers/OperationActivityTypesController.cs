﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralOperations;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class OperationActivityTypesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppSettings _appSettings;

        public OperationActivityTypesController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;
            _appSettings = settings.Value;
        }

        // GET: OperationActivityTypes
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var qry = _context.OperationActivityTypes.AsNoTracking().OrderByDescending(x => x.Id);

            if (!string.IsNullOrEmpty(search))
            {
                qry = qry.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);
            }

            var model = await PagingList.CreateAsync(qry, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: OperationActivityTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var operationActivityType = await _context.OperationActivityTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (operationActivityType == null)
            {
                return NotFound();
            }

            return View(operationActivityType);
        }

        // GET: OperationActivityTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OperationActivityTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Description")] OperationActivityType operationActivityType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(operationActivityType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(operationActivityType);
        }

        // GET: OperationActivityTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var operationActivityType = await _context.OperationActivityTypes.SingleOrDefaultAsync(m => m.Id == id);
            if (operationActivityType == null)
            {
                return NotFound();
            }
            return View(operationActivityType);
        }

        // POST: OperationActivityTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Description")] OperationActivityType operationActivityType)
        {
            if (id != operationActivityType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(operationActivityType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OperationActivityTypeExists(operationActivityType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(operationActivityType);
        }

        // GET: OperationActivityTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var operationActivityType = await _context.OperationActivityTypes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (operationActivityType == null)
            {
                return NotFound();
            }

            return View(operationActivityType);
        }

        // POST: OperationActivityTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var operationActivityType = await _context.OperationActivityTypes.SingleOrDefaultAsync(m => m.Id == id);
            _context.OperationActivityTypes.Remove(operationActivityType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.OperationActivityTypes.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool OperationActivityTypeExists(int id)
        {
            return _context.OperationActivityTypes.Any(e => e.Id == id);
        }
    }
}
