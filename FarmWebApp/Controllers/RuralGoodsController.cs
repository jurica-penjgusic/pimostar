﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  29.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Config;
using FarmWebApp.Data;
using FarmWebApp.Models.RuralGoods;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReflectionIT.Mvc.Paging;

namespace FarmWebApp.Controllers
{
    public class RuralGoodsController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly ApplicationDbContext _context;

        public RuralGoodsController(ApplicationDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;
            _appSettings = settings.Value;
        }

        // GET: RuralGoods
        public async Task<IActionResult> Index(int page = 1, string search = null)
        {
            var query = _context.RuralGoods.Include(r => r.RuralGoodCategory).AsNoTracking()
                .OrderByDescending(x => x.Id);


            if (!string.IsNullOrEmpty(search))
                query = query.Where(x => x.Name.Contains(search) || x.Description.Contains(search))
                    .OrderByDescending(x => x.Id);

            var model = await PagingList.CreateAsync(query, _appSettings.PageSize, page);

            model.RouteValue = new RouteValueDictionary
            {
                {"Search", search}
            };

            return View(model);
        }

        // GET: RuralGoods/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralGood = await _context.RuralGoods
                .Include(r => r.RuralGoodCategory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGood == null)
                return NotFound();

            return View(ruralGood);
        }

        // GET: RuralGoods/Create
        public IActionResult Create()
        {
            ViewData["RuralCategoryId"] = new SelectList(_context.RuralGoodCategories, "Id", "Name");
            return View();
        }

        // POST: RuralGoods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Id,Name,Description,RuralCategoryId,Active")] RuralGood ruralGood)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruralGood);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RuralCategoryId"] =
                new SelectList(_context.RuralGoodCategories, "Id", "Id", ruralGood.RuralCategoryId);
            return View(ruralGood);
        }

        // GET: RuralGoods/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralGood = await _context.RuralGoods.SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGood == null)
                return NotFound();
            ViewData["RuralCategoryId"] =
                new SelectList(_context.RuralGoodCategories, "Id", "Name", ruralGood.RuralCategoryId);
            return View(ruralGood);
        }

        // POST: RuralGoods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,Name,Description,RuralCategoryId,Active")] RuralGood ruralGood)
        {
            if (id != ruralGood.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ruralGood);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuralGoodExists(ruralGood.Id))
                        return NotFound();
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RuralCategoryId"] =
                new SelectList(_context.RuralGoodCategories, "Id", "Name", ruralGood.RuralCategoryId);
            return View(ruralGood);
        }

        // GET: RuralGoods/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var ruralGood = await _context.RuralGoods
                .Include(r => r.RuralGoodCategory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ruralGood == null)
                return NotFound();

            return View(ruralGood);
        }

        // POST: RuralGoods/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruralGood = await _context.RuralGoods.SingleOrDefaultAsync(m => m.Id == id);
            _context.RuralGoods.Remove(ruralGood);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AutoCompleteResult(string search)
        {
            var result = await _context.RuralGoods.Where(x => x.Name.Contains(search)).Take(_appSettings.AutoCompleteCount).ToListAsync();

            return Json(result);
        }

        private bool RuralGoodExists(int id)
        {
            return _context.RuralGoods.Any(e => e.Id == id);
        }
    }
}