﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  31.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Data;
using FarmWebApp.Models.ComplexModels;
using FarmWebApp.Models.ToolsAndMechanism;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FarmWebApp.Controllers
{
    public class ToolsPerRuralPropertiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ToolsPerRuralPropertiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ToolsPerRuralProperties
        public async Task<IActionResult> Index(int? id, int? toolId)
        {
            var viewModel = new ToolsAndMechenismPerRuralProperty();

            viewModel.ToolsPerRuralProperties = await _context.ToolsPerRuralProperties
                .Include(x => x.RuralProperty)
                .ThenInclude(x => x.Company)
                .Include(x => x.RuralPropertyActivity)
                .Include(x => x.ToolsAndMechanism)
                .ThenInclude(x => x.ToolCosts)
                .ThenInclude(x => x.ToolCostType)
                .Include(x => x.ToolsAndMechanism).ThenInclude(x => x.Company).ToListAsync();

            if (id != null)
            {
                ViewData["RuralPropertyId"] = id.Value;
                var tool = viewModel.ToolsPerRuralProperties.Where(i => i.RuralProperty.Id == id)
                    .Select(x => x.ToolsAndMechanism).ToList();

                viewModel.ToolsAndMechanisms = tool;
            }

            if (toolId != null)
            {
                ViewData["ToolAndMechanismId"] = toolId.Value;
                var tool = viewModel.ToolsAndMechanisms.FirstOrDefault(i => i.Id == toolId)?.ToolCosts;

                viewModel.ToolCosts = tool;
            }

            return View(viewModel);
        }

        // GET: ToolsPerRuralProperties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var toolsPerRuralProperty = await _context.ToolsPerRuralProperties
                .Include(t => t.RuralProperty)
                .Include(t => t.RuralPropertyActivity)
                .Include(t => t.ToolsAndMechanism)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolsPerRuralProperty == null)
                return NotFound();

            return View(toolsPerRuralProperty);
        }

        // GET: ToolsPerRuralProperties/Create
        public IActionResult Create()
        {
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Name");
            ViewData["RuralPropertyActivityId"] = new SelectList(_context.RuralPropertyActivities, "Id", "Name");
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Name");
            return View();
        }

        // POST: ToolsPerRuralProperties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Id,ToolId,RuralPropertyId,From,To,RuralPropertyActivityId")]
            ToolsPerRuralProperty toolsPerRuralProperty)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toolsPerRuralProperty);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Id",
                toolsPerRuralProperty.RuralPropertyId);
            ViewData["RuralPropertyActivityId"] = new SelectList(_context.RuralPropertyActivities, "Id", "Id",
                toolsPerRuralProperty.RuralPropertyActivityId);
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolsPerRuralProperty.ToolId);
            return View(toolsPerRuralProperty);
        }

        // GET: ToolsPerRuralProperties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var toolsPerRuralProperty = await _context.ToolsPerRuralProperties.SingleOrDefaultAsync(m => m.Id == id);
            if (toolsPerRuralProperty == null)
                return NotFound();
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Id",
                toolsPerRuralProperty.RuralPropertyId);
            ViewData["RuralPropertyActivityId"] = new SelectList(_context.RuralPropertyActivities, "Id", "Id",
                toolsPerRuralProperty.RuralPropertyActivityId);
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolsPerRuralProperty.ToolId);
            return View(toolsPerRuralProperty);
        }

        // POST: ToolsPerRuralProperties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,ToolId,RuralPropertyId,From,To,RuralPropertyActivityId")]
            ToolsPerRuralProperty toolsPerRuralProperty)
        {
            if (id != toolsPerRuralProperty.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toolsPerRuralProperty);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToolsPerRuralPropertyExists(toolsPerRuralProperty.Id))
                        return NotFound();
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RuralPropertyId"] = new SelectList(_context.RuralProperties, "Id", "Id",
                toolsPerRuralProperty.RuralPropertyId);
            ViewData["RuralPropertyActivityId"] = new SelectList(_context.RuralPropertyActivities, "Id", "Id",
                toolsPerRuralProperty.RuralPropertyActivityId);
            ViewData["ToolId"] = new SelectList(_context.ToolsAndMechanisms, "Id", "Id", toolsPerRuralProperty.ToolId);
            return View(toolsPerRuralProperty);
        }

        // GET: ToolsPerRuralProperties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var toolsPerRuralProperty = await _context.ToolsPerRuralProperties
                .Include(t => t.RuralProperty)
                .Include(t => t.RuralPropertyActivity)
                .Include(t => t.ToolsAndMechanism)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (toolsPerRuralProperty == null)
                return NotFound();

            return View(toolsPerRuralProperty);
        }

        // POST: ToolsPerRuralProperties/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toolsPerRuralProperty = await _context.ToolsPerRuralProperties.SingleOrDefaultAsync(m => m.Id == id);
            _context.ToolsPerRuralProperties.Remove(toolsPerRuralProperty);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToolsPerRuralPropertyExists(int id)
        {
            return _context.ToolsPerRuralProperties.Any(e => e.Id == id);
        }
    }
}