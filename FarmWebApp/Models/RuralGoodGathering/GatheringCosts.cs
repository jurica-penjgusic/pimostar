﻿using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.RuralGoodGathering
{
    public class GatheringCosts
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Naziv")]
        public string Name { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }
    }
}