﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FarmWebApp.Models.RuralGoods;

namespace FarmWebApp.Models.RuralGoodGathering
{
    public class RuralGoodGathering
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Dobro po imanju")]
        public int RuralGoodPerPropertyId { get; set; }

        [Display(Name = "Dobro po imanju")]
        [ForeignKey("RuralGoodPerPropertyId")]
        public RuralGoodPerProperty RuralGoodPerProperty { get; set; }

        [Display(Name = "Mjerna Jedinica")]
        public string MeasurementUnit { get; set; }

        [Display(Name = "Količina")]
        public decimal Amount { get; set; }

        [Display(Name = "Vrijeme berbe")]
        public DateTime GatheringTime { get; set; }

        [Display(Name = "Troskovi Berbe")]
        [ForeignKey("GatheringCostId")]
        public GatheringCosts GatheringCosts { get; set; }

        [Display(Name = "Koristena Mehanizacija")]
        public ICollection<ToolsAndMechanism.ToolsAndMechanism> ToolsAndMechanismsUsed { get; set; }

        public int GatheringCostId { get; set; }

        [Display(Name = "Sezona")]
        [ForeignKey("SeasonId")]
        public Season.Season Season { get; set; }

        [Display(Name = "Sezona")]
        public int? SeasonId { get; set; }
    }
}