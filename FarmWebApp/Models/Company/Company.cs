﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.Company
{
    public class Company
    {
        public Company()
        {
            Users = new List<ApplicationUser>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public string Description { get; set; }

        public int CompanyTypeId { get; set; }

        [ForeignKey("CompanyTypeId")]
        public CompanyType CompanyType { get; set; }

        public ICollection<ApplicationUser> Users { get; set; }
    }
}