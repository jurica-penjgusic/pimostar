﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.Company
{
    public class CompanyType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}