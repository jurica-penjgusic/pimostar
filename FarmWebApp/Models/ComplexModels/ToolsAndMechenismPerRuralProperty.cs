﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Models.ToolsAndMechanism;

namespace FarmWebApp.Models.ComplexModels
{
    public class ToolsAndMechenismPerRuralProperty
    {
        public IEnumerable<ToolsAndMechanism.ToolsAndMechanism> ToolsAndMechanisms { get; set; }

        public IEnumerable<ToolsPerRuralProperty> ToolsPerRuralProperties { get; set; }

        public IEnumerable<ToolCost> ToolCosts { get; set; }
    }
}
