﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.SalePlaces
{
    /// <summary>
    /// Company Sale Place
    /// </summary>
    public class CompanySalePlace
    {
        [Key]
        public int Id { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public Company.Company Company { get; set; }

        [StringLength(128)]
        public string SalePlaceName { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        [ForeignKey("SalePlaceCategoryId")]
        public SalePlaceCatogory SalePlaceCatogory { get; set; }

        public int SalePlaceCategoryId { get; set; }
    }
}