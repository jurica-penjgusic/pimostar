﻿namespace FarmWebApp.Models.MlijekoMati
{
    public class Trosak
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Opis { get; set; }
    }
}