﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.MlijekoMati
{
    public class TroskoviMlijekomata
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Mlijekomat")]
        public int MlijekomatId { get; set; }

        [Display(Name = "Mlijekomat")]
        [ForeignKey("MlijekomatId")]
        public MlijekoMat MlijekoMat { get; set; }

        public string Valuta { get; set; }

        public decimal Iznos { get; set; }

        [Display(Name = "Datum Plaćanja")]
        [DataType(DataType.Date)]
        public DateTime DatumPlacanja { get; set; }

        [Display(Name = "Tip Troška")]
        public int TipTroskaId { get; set; }

        [Display(Name = "Tip Troška")]
        [ForeignKey("TipTroskaId")]
        public Trosak TipTroska { get; set; }

        [Display(Name = "Cijena Po Jedinici")]
        public decimal CijenaPoJedinici { get; set; }

        [Display(Name = "Ukupan Novac")]
        public decimal UkupanNovac { get; set; }
    }
}