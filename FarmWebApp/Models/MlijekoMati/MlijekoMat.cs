﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using FarmWebApp.Controllers;

namespace FarmWebApp.Models.MlijekoMati
{
    public class MlijekoMat
    {
        [Key]
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Lokacija { get; set; }

        [Display(Name= "Lokalni Naziv")]
        public string LokalniNaziv { get; set; }

        [Display(Name = "Vlasnik")]
        public int CompanyId { get; set; }

        [Display(Name = "Vlasnik")]
        [ForeignKey("CompanyId")]
        public Company.Company Company { get; set; }

        public ICollection<Trosak> TipTroskova { get; set; }

        public ICollection<TroskoviMlijekomata> Troskovi { get; set; }

        public ICollection<PrihodiMlijekomata> Prihod { get; set; }

    }
}
