﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FarmWebApp.Models.MlijekoMati
{
    public class PrihodiMlijekomata
    {
        public int Id { get; set; }

        [Display(Name = "Mlijekomat")]
        [ForeignKey("MlijekomatId")]
        public MlijekoMat MlijekoMat { get; set; }

        [Display(Name = "Mlijekomat")]
        public int MlijekomatId { get; set; }

        [Display(Name = "Prodana Količina")]
        public decimal ProdanaKolicina { get; set; }

        [Display(Name = "Vraćena Količina")]
        public decimal VracenaKolicina { get; set; }

        [Display(Name = "Tip Radnje")]
        [ForeignKey("TipRadnjeId")]
        public TipRadnje TipRadnje { get; set; }

        [Display(Name = "Tip Radnje")]
        public int TipRadnjeId { get; set; }
    }
}
