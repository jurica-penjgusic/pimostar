﻿using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.MlijekoMati
{
    public class TipRadnje  
    {
        [Key]
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Opis { get; set; }
    }
}