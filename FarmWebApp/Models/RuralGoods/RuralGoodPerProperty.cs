﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FarmWebApp.Models.RuralProperties;

namespace FarmWebApp.Models.RuralGoods
{
    public class RuralGoodPerProperty
    {
        [Key]
        public int Id { get; set; }

        public int RuralGoodId { get; set; }

        [ForeignKey("RuralGoodId")]
        public RuralGood RuralGood { get; set; }

        public int RuralPropertyId { get; set; }

        [ForeignKey("RuralPropertyId")]
        public RuralProperty RuralProperty { get; set; }

        // Logical this will be called year
        // But in some cases there are plants that grow every 4 months 3 or 5... 
        public DateTime TimeOfProductPerLand { get; set; }

        public string MeasurementUnit { get; set; }

        public decimal Amount { get; set; }

        public string Name => RuralGood?.Name;
    }
}