﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.RuralGoods
{
    public class RuralGood
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey("RuralCategoryId")]
        public RuralGoodCategory RuralGoodCategory { get; set; }

        public int RuralCategoryId { get; set; }

        public bool Active { get; set; }

        public ICollection<RuralGoodCosts> RuralGoodCosts { get; set; }
    }
}