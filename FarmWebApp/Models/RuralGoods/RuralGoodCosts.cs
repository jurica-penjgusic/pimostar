﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.RuralGoods
{
    public class RuralGoodCosts
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Currency { get; set; }

        public decimal Amount { get; set; }

        public DateTime Created { get; set; }

        [ForeignKey("RuralCostTypeId")]
        public RuralCostType RuralCostType { get; set; }

        public int RuralCostTypeId { get; set; }
    }
}