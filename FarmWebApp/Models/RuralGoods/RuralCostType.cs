﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.RuralGoods
{
    public class RuralCostType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}