﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.ComponentModel.DataAnnotations.Schema;
using FarmWebApp.Models.RuralProperties;

namespace FarmWebApp.Models.RuralGoods
{
    public class PotentialRuralGoods
    {
        [ForeignKey("RuralPropertyId")]
        public RuralProperty RuralProperty { get; set; }

        [ForeignKey("RuralGoodId")]
        public RuralGood RuralGood { get; set; }

        public int RuralGoodId { get; set; }

        public int RuralPropertyId { get; set; }
    }
}