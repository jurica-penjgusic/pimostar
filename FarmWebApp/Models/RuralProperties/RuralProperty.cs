﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.RuralProperties
{
    public class RuralProperty
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string LocalIdentifier { get; set; }

        public double Longitute { get; set; }

        public double Latitude { get; set; }

        public string Description { get; set; }

        public string Size { get; set; }

        public string SizeMeasurementUnit { get; set; }

        [ForeignKey("CompanyId")]
        public Company.Company Company { get; set; }

        public int CompanyId { get; set; }

        public int RuralPropertyTypeId { get; set; }

        [ForeignKey("RuralPropertyTypeId")]
        public RuralPropertyType RuralPropertyType { get; set; }
    }
}