﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Extensions;

namespace FarmWebApp.Models.Season
{
    public enum SeasonEnum
    {
        Winter,
        Spring,
        Summer,
        Autumn
    }

    public class Season
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Description { get; set; }
       
    }
}
