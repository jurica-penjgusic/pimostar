﻿using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.RuralOperations
{
    public class OperationActivityType  
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string  Description { get; set; }
    }
}