﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.RuralOperations
{
    public class OperationType
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Naziv Tipa radnje")]
        public string Name { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Sezona")]
        [ForeignKey("SeasonId")]
        public Season.Season Season { get; set; }

        [Display(Name = "Sezona")]
        public int? SeasonId { get; set; }
    }
}