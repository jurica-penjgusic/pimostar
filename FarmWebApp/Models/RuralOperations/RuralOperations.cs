﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FarmWebApp.Models.RuralProperties;

namespace FarmWebApp.Models.RuralOperations
{
    public class RuralOperations
    {
        public RuralOperations()
        {
            ToolsPerRuralOperations = new List<ToolsPerRuralOperation>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Imanje")]
        public int RuralPropertyId { get; set; }

        [Display(Name = "Imanje")]
        [ForeignKey("RuralPropertyId")]
        public RuralProperty RuralProperty { get; set; }

        [Display(Name = "Naziv Radnje")]
        public string Name { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Vrijeme radnje")]
        public DateTime OperationDateTime { get; set; }

        [Display(Name = "Aktivnost")]
        [ForeignKey("OperationActivityTypeId")]
        public OperationActivityType OperationActivityType { get; set; }

        [Display(Name = "Aktivnost")]
        public int OperationActivityTypeId { get; set; }

        [Display(Name = "Tip Radnje")]
        [ForeignKey("OperationTypeId")]
        public OperationType OperationType { get; set; }

        [Display(Name = "Tip Radnje")]
        public int OperationTypeId { get; set; }

        [Display(Name = "Valuta")]
        public string Currency { get; set; }

        [Display(Name = "Koristeni alati")]
        public ICollection<ToolsPerRuralOperation> ToolsPerRuralOperations { get; set; }

        [Display(Name = "Mjerna Jedinica")]
        public string MeasurementUnit { get; set; }

        [Display(Name = "Cijena po jedinici")]
        public decimal? PricePerUnit { get; set; }

        [Display(Name = "Ukupan trosak")]
        // This will be total cost
        public decimal? Cost { get; set; }
    }
}