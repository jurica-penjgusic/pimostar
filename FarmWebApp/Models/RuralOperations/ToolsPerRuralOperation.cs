﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.RuralOperations
{
    public class ToolsPerRuralOperation
    {
        [Key]
        public int ToolId { get; set; }

        [ForeignKey("ToolId")]
        public ToolsAndMechanism.ToolsAndMechanism ToolsAndMechanism { get; set; }

        public string Currency { get; set; }

        public decimal? Amount { get; set; }
    }
}
