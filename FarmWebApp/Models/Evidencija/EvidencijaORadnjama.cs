﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Models.RuralGoods;

namespace FarmWebApp.Models.Evidencija
{
    public class EvidencijaORadnjama
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Naziv")]
        public string Name { get; set; }

        [Display(Name = "Gospodarsko Dobro")]
        public int? GospodarskoDobroId { get; set; }

        [Display(Name = "Gospodarsko Dobro")]
        [ForeignKey("GospodarskoDobroId")]
        public RuralGood GospodarskoDobro { get; set; }

        [Display(Name = "Datum Izvršenja")]
        [DataType(DataType.Date)]
        public DateTime DatumIzvrsenja { get; set; }

        [Display(Name = "Tip Aktivnosti")]
        [ForeignKey("TipAktivnostiId")]
        public TipAktivnosti TipAktivnosti { get; set; }
        
        [Display(Name = "Tip Aktivnosti")]
        public int? TipAktivnostiId { get; set; }

        public string Valuta { get; set; }

        public decimal Cijena { get; set; }

    }
}
