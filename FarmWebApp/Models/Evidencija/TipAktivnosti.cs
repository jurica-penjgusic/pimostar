﻿using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.Evidencija
{
    public class TipAktivnosti
    {
        [Key]
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Opis { get; set; }

    }
}