﻿using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.Evidencija
{
    public class TipPracenja
    {
        [Key]
        public int Id { get; set; }

        public string  Naziv { get; set; }

        public string Opis { get; set; }
    }
}