﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using FarmWebApp.Models.RuralGoods;

namespace FarmWebApp.Models.Evidencija
{
    public class Evidencija
    {
        [Key]
        public int Id { get; set; }

        public string NazivEvidencije { get; set; }

        public int? GospodarskoDobroId { get; set; }

        [ForeignKey("GospodarskoDobroId")]
        public RuralGood GosparskoDobro { get; set; }

        public int? TipPracenjaId { get; set; }

        [ForeignKey("TipPracenjaId")]
        public TipPracenja TipPracenja { get; set; }

        public string MjernaJedinica { get; set; }

        public decimal PrethodnoStanje { get; set; }

        public decimal TrenutnoStanje { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DatumZapisa { get; set; }
    }
}
