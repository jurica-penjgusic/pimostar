﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmWebApp.Models.ToolsAndMechanism
{
    public class ToolsAndMechanism
    {
        [Key]
        public int Id { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public Company.Company Company { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string UnitMeasurement { get; set; }

        public int Amount { get; set; }

        public ICollection<ToolCost> ToolCosts { get; set; }

        public ICollection<ToolsPerRuralProperty> ToolsPerRuralProperty { get; set; }

        public ToolsAndMechanism()
        {
            ToolCosts = new List<ToolCost>();
        }

    }
}