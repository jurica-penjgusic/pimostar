﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FarmWebApp.Models.RuralProperties;

namespace FarmWebApp.Models.ToolsAndMechanism
{
    public class ToolsPerRuralProperty
    {
        [Key]
        public int Id { get; set; }

        public int? ToolId { get; set; }

        [ForeignKey("ToolId")]
        public ToolsAndMechanism ToolsAndMechanism { get; set; }

        [ForeignKey("RuralPropertyId")]
        public RuralProperty RuralProperty { get; set; }

        public int? RuralPropertyId { get; set; }

        [DataType(DataType.Date)]
        public DateTime From { get; set; }

        [DataType(DataType.Date)]
        public DateTime To { get; set; }

        [ForeignKey("RuralPropertyActivityId")]
        public RuralPropertyActivity RuralPropertyActivity { get; set; }

        public int RuralPropertyActivityId { get; set; }

        public ToolsPerRuralProperty()
        {
            From = DateTime.Now.Date;
            To = DateTime.Now.Date.AddDays(1);
        }
    }
}