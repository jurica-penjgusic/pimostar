﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FarmWebApp.Models.ToolsAndMechanism
{
    public class ToolCost
    {
        [Key]
        public int Id { get; set; }

        public int ToolsAndMechanismId { get; set; }

        [ForeignKey("ToolsAndMechanismId")]
        public ToolsAndMechanism ToolsAndMechanism { get; set; }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime CostTime { get; set; }

        public double Amount { get; set; }

        public string Currency { get; set; }

        public int CostTypeId { get; set; }

        [ForeignKey("CostTypeId")]
        public ToolCostType ToolCostType { get; set; }
    }
}
