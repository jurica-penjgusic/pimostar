﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System.ComponentModel.DataAnnotations;

namespace FarmWebApp.Models.ToolsAndMechanism
{
    public class ToolCostType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}