﻿// ////////////////////////////////////////////////////////
//     Created By: Vlado Boškić                     
//     Project: FarmWebApp                             
//     Created:  28.05.2018            
//                                                                
// ///////////////////////////////////////////////////////

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;

namespace FarmWebApp.Models.ToolsAndMechanism
{
    public class ToolsOutsourced
    {
        [Key]
        public int Id { get; set; }

        public int? ToolId { get; set; }

        [Display(Name = "Vlasnik")]
        public int? CompanyId { get; set; }

        [Display(Name = "Vlasnik")]
        [ForeignKey("CompanyId")]
        public Company.Company Company { get; set; }

        [Display(Name = "Alati i Mehanizacija")]
        [ForeignKey("ToolId")]
        public ToolsAndMechanism ToolsAndMechanism { get; set; }

        [Display(Name = "Datum početka korištenja")]
        [DataType(DataType.Date)]
        public DateTime From { get; set; }

        [Display(Name = "Kraj korištenja")]
        [DataType(DataType.Date)]
        public DateTime To { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Valuta")]
        public string Currency { get; set; }

        [Display(Name = "Cijena")]
        public decimal Price { get; set; }
    }
}