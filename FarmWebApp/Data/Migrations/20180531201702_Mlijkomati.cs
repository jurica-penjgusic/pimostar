﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class Mlijkomati : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MlijekoMati",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    Lokacija = table.Column<string>(nullable: true),
                    LokalniNaziv = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MlijekoMati", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MlijekoMati_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TipRadnje",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(nullable: true),
                    Opis = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipRadnje", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trosak",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MlijekoMatId = table.Column<int>(nullable: true),
                    Naziv = table.Column<string>(nullable: true),
                    Opis = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trosak", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trosak_MlijekoMati_MlijekoMatId",
                        column: x => x.MlijekoMatId,
                        principalTable: "MlijekoMati",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PrihodiMlijekomata",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MlijekomatId = table.Column<int>(nullable: false),
                    ProdanaKolicina = table.Column<decimal>(nullable: false),
                    TipRadnjeId = table.Column<int>(nullable: false),
                    VracenaKolicina = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrihodiMlijekomata", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrihodiMlijekomata_MlijekoMati_MlijekomatId",
                        column: x => x.MlijekomatId,
                        principalTable: "MlijekoMati",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrihodiMlijekomata_TipRadnje_TipRadnjeId",
                        column: x => x.TipRadnjeId,
                        principalTable: "TipRadnje",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TroskoviMlijekomata",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CijenaPoJedinici = table.Column<decimal>(nullable: false),
                    DatumPlacanja = table.Column<DateTime>(nullable: false),
                    Iznos = table.Column<decimal>(nullable: false),
                    MlijekomatId = table.Column<int>(nullable: false),
                    TipTroskaId = table.Column<int>(nullable: false),
                    UkupanNovac = table.Column<decimal>(nullable: false),
                    Valuta = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TroskoviMlijekomata", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TroskoviMlijekomata_MlijekoMati_MlijekomatId",
                        column: x => x.MlijekomatId,
                        principalTable: "MlijekoMati",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TroskoviMlijekomata_Trosak_TipTroskaId",
                        column: x => x.TipTroskaId,
                        principalTable: "Trosak",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MlijekoMati_CompanyId",
                table: "MlijekoMati",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_PrihodiMlijekomata_MlijekomatId",
                table: "PrihodiMlijekomata",
                column: "MlijekomatId");

            migrationBuilder.CreateIndex(
                name: "IX_PrihodiMlijekomata_TipRadnjeId",
                table: "PrihodiMlijekomata",
                column: "TipRadnjeId");

            migrationBuilder.CreateIndex(
                name: "IX_Trosak_MlijekoMatId",
                table: "Trosak",
                column: "MlijekoMatId");

            migrationBuilder.CreateIndex(
                name: "IX_TroskoviMlijekomata_MlijekomatId",
                table: "TroskoviMlijekomata",
                column: "MlijekomatId");

            migrationBuilder.CreateIndex(
                name: "IX_TroskoviMlijekomata_TipTroskaId",
                table: "TroskoviMlijekomata",
                column: "TipTroskaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrihodiMlijekomata");

            migrationBuilder.DropTable(
                name: "TroskoviMlijekomata");

            migrationBuilder.DropTable(
                name: "TipRadnje");

            migrationBuilder.DropTable(
                name: "Trosak");

            migrationBuilder.DropTable(
                name: "MlijekoMati");
        }
    }
}
