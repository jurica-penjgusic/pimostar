﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "UserNameIndex",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUserRoles_UserId",
                table: "AspNetUserRoles");

            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles");

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CompanyTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OperationActivityTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationActivityTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OperationTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuralCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuralCostTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralCostTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuralGoodCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralGoodCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuralPropertyActivities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralPropertyActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuralPropertyTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralPropertyTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SalePlaceCatogories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalePlaceCatogories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Seasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ToolCostType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolCostType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ToolsAndMechanisms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    UnitMeasurement = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolsAndMechanisms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ToolsOutsources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Currency = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    From = table.Column<DateTime>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    To = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolsOutsources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyTypeId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Companies_CompanyTypes_CompanyTypeId",
                        column: x => x.CompanyTypeId,
                        principalTable: "CompanyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RuralOperations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cost = table.Column<decimal>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    MeasurementUnit = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OperationActivityTypeId = table.Column<int>(nullable: false),
                    OperationDateTime = table.Column<DateTime>(nullable: false),
                    OperationTypeId = table.Column<int>(nullable: false),
                    PricePerUnit = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralOperations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuralOperations_OperationActivityTypes_OperationActivityTypeId",
                        column: x => x.OperationActivityTypeId,
                        principalTable: "OperationActivityTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuralOperations_OperationTypes_OperationTypeId",
                        column: x => x.OperationTypeId,
                        principalTable: "OperationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RuralGoods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    RuralCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralGoods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuralGoods_RuralGoodCategories_RuralCategoryId",
                        column: x => x.RuralCategoryId,
                        principalTable: "RuralGoodCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanySalePlaces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    SalePlaceCategoryId = table.Column<int>(nullable: false),
                    SalePlaceName = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanySalePlaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanySalePlaces_SalePlaceCatogories_SalePlaceCategoryId",
                        column: x => x.SalePlaceCategoryId,
                        principalTable: "SalePlaceCatogories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ToolCost",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<double>(nullable: false),
                    CostTime = table.Column<DateTime>(nullable: false),
                    CostTypeId = table.Column<int>(nullable: false),
                    Currency = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolCost", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ToolCost_ToolCostType_CostTypeId",
                        column: x => x.CostTypeId,
                        principalTable: "ToolCostType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RuralProperties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    LocalIdentifier = table.Column<string>(nullable: true),
                    Longitute = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    RuralPropertyTypeId = table.Column<int>(nullable: false),
                    Size = table.Column<string>(nullable: true),
                    SizeMeasurementUnit = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuralProperties_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuralProperties_RuralPropertyTypes_RuralPropertyTypeId",
                        column: x => x.RuralPropertyTypeId,
                        principalTable: "RuralPropertyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ToolsPerRuralOperation",
                columns: table => new
                {
                    ToolId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    RuralOperationsId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolsPerRuralOperation", x => x.ToolId);
                    table.ForeignKey(
                        name: "FK_ToolsPerRuralOperation_RuralOperations_RuralOperationsId",
                        column: x => x.RuralOperationsId,
                        principalTable: "RuralOperations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ToolsPerRuralOperation_ToolsAndMechanisms_ToolId",
                        column: x => x.ToolId,
                        principalTable: "ToolsAndMechanisms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RuralGoodCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Currency = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    RuralCostTypeId = table.Column<int>(nullable: false),
                    RuralGoodId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralGoodCosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuralGoodCosts_RuralCostTypes_RuralCostTypeId",
                        column: x => x.RuralCostTypeId,
                        principalTable: "RuralCostTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuralGoodCosts_RuralGoods_RuralGoodId",
                        column: x => x.RuralGoodId,
                        principalTable: "RuralGoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RuralGoodPerProperties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    MeasurementUnit = table.Column<string>(nullable: true),
                    RuralGoodId = table.Column<int>(nullable: false),
                    RuralPropertyId = table.Column<int>(nullable: false),
                    TimeOfProductPerLand = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralGoodPerProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuralGoodPerProperties_RuralGoods_RuralGoodId",
                        column: x => x.RuralGoodId,
                        principalTable: "RuralGoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuralGoodPerProperties_RuralProperties_RuralPropertyId",
                        column: x => x.RuralPropertyId,
                        principalTable: "RuralProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ToolsPerRuralProperties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    From = table.Column<DateTime>(nullable: false),
                    RuralPropertyActivityId = table.Column<int>(nullable: false),
                    RuralPropertyId = table.Column<int>(nullable: false),
                    To = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolsPerRuralProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ToolsPerRuralProperties_RuralPropertyActivities_RuralPropertyActivityId",
                        column: x => x.RuralPropertyActivityId,
                        principalTable: "RuralPropertyActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                        column: x => x.RuralPropertyId,
                        principalTable: "RuralProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CompanyTypeId",
                table: "Companies",
                column: "CompanyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanySalePlaces_SalePlaceCategoryId",
                table: "CompanySalePlaces",
                column: "SalePlaceCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodCosts_RuralCostTypeId",
                table: "RuralGoodCosts",
                column: "RuralCostTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodCosts_RuralGoodId",
                table: "RuralGoodCosts",
                column: "RuralGoodId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodPerProperties_RuralGoodId",
                table: "RuralGoodPerProperties",
                column: "RuralGoodId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodPerProperties_RuralPropertyId",
                table: "RuralGoodPerProperties",
                column: "RuralPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoods_RuralCategoryId",
                table: "RuralGoods",
                column: "RuralCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralOperations_OperationActivityTypeId",
                table: "RuralOperations",
                column: "OperationActivityTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralOperations_OperationTypeId",
                table: "RuralOperations",
                column: "OperationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralProperties_CompanyId",
                table: "RuralProperties",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralProperties_RuralPropertyTypeId",
                table: "RuralProperties",
                column: "RuralPropertyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ToolCost_CostTypeId",
                table: "ToolCost",
                column: "CostTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ToolsPerRuralOperation_RuralOperationsId",
                table: "ToolsPerRuralOperation",
                column: "RuralOperationsId");

            migrationBuilder.CreateIndex(
                name: "IX_ToolsPerRuralProperties_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_ToolsPerRuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                table: "AspNetUserTokens",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                table: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CompanySalePlaces");

            migrationBuilder.DropTable(
                name: "RuralCategories");

            migrationBuilder.DropTable(
                name: "RuralGoodCosts");

            migrationBuilder.DropTable(
                name: "RuralGoodPerProperties");

            migrationBuilder.DropTable(
                name: "Seasons");

            migrationBuilder.DropTable(
                name: "ToolCost");

            migrationBuilder.DropTable(
                name: "ToolsOutsources");

            migrationBuilder.DropTable(
                name: "ToolsPerRuralOperation");

            migrationBuilder.DropTable(
                name: "ToolsPerRuralProperties");

            migrationBuilder.DropTable(
                name: "SalePlaceCatogories");

            migrationBuilder.DropTable(
                name: "RuralCostTypes");

            migrationBuilder.DropTable(
                name: "RuralGoods");

            migrationBuilder.DropTable(
                name: "ToolCostType");

            migrationBuilder.DropTable(
                name: "RuralOperations");

            migrationBuilder.DropTable(
                name: "ToolsAndMechanisms");

            migrationBuilder.DropTable(
                name: "RuralPropertyActivities");

            migrationBuilder.DropTable(
                name: "RuralProperties");

            migrationBuilder.DropTable(
                name: "RuralGoodCategories");

            migrationBuilder.DropTable(
                name: "OperationActivityTypes");

            migrationBuilder.DropTable(
                name: "OperationTypes");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "RuralPropertyTypes");

            migrationBuilder.DropTable(
                name: "CompanyTypes");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "UserNameIndex",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "AspNetUsers");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_UserId",
                table: "AspNetUserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName");
        }
    }
}
