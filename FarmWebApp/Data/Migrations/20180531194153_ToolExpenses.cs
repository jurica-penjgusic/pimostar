﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class ToolExpenses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost");

            migrationBuilder.AlterColumn<int>(
                name: "ToolsAndMechanismId",
                table: "ToolCost",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost",
                column: "ToolsAndMechanismId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost");

            migrationBuilder.AlterColumn<int>(
                name: "ToolsAndMechanismId",
                table: "ToolCost",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost",
                column: "ToolsAndMechanismId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
