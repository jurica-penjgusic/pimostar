﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class Gathering : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RuralGoodGatheringId",
                table: "ToolsAndMechanisms",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GatheringCosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GatheringCosts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuralGoodGatherings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    GatheringCostId = table.Column<int>(nullable: false),
                    GatheringTime = table.Column<DateTime>(nullable: false),
                    MeasurementUnit = table.Column<string>(nullable: true),
                    RuralGoodPerPropertyId = table.Column<int>(nullable: false),
                    SeasonId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralGoodGatherings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RuralGoodGatherings_GatheringCosts_GatheringCostId",
                        column: x => x.GatheringCostId,
                        principalTable: "GatheringCosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuralGoodGatherings_RuralGoodPerProperties_RuralGoodPerPropertyId",
                        column: x => x.RuralGoodPerPropertyId,
                        principalTable: "RuralGoodPerProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuralGoodGatherings_Seasons_SeasonId",
                        column: x => x.SeasonId,
                        principalTable: "Seasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ToolsAndMechanisms_RuralGoodGatheringId",
                table: "ToolsAndMechanisms",
                column: "RuralGoodGatheringId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodGatherings_GatheringCostId",
                table: "RuralGoodGatherings",
                column: "GatheringCostId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodGatherings_RuralGoodPerPropertyId",
                table: "RuralGoodGatherings",
                column: "RuralGoodPerPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralGoodGatherings_SeasonId",
                table: "RuralGoodGatherings",
                column: "SeasonId");

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsAndMechanisms_RuralGoodGatherings_RuralGoodGatheringId",
                table: "ToolsAndMechanisms",
                column: "RuralGoodGatheringId",
                principalTable: "RuralGoodGatherings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolsAndMechanisms_RuralGoodGatherings_RuralGoodGatheringId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropTable(
                name: "RuralGoodGatherings");

            migrationBuilder.DropTable(
                name: "GatheringCosts");

            migrationBuilder.DropIndex(
                name: "IX_ToolsAndMechanisms_RuralGoodGatheringId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropColumn(
                name: "RuralGoodGatheringId",
                table: "ToolsAndMechanisms");
        }
    }
}
