﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class OperationTypeSeason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SeasonId",
                table: "OperationTypes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OperationTypes_SeasonId",
                table: "OperationTypes",
                column: "SeasonId");

            migrationBuilder.AddForeignKey(
                name: "FK_OperationTypes_Seasons_SeasonId",
                table: "OperationTypes",
                column: "SeasonId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OperationTypes_Seasons_SeasonId",
                table: "OperationTypes");

            migrationBuilder.DropIndex(
                name: "IX_OperationTypes_SeasonId",
                table: "OperationTypes");

            migrationBuilder.DropColumn(
                name: "SeasonId",
                table: "OperationTypes");
        }
    }
}
