﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class ToolCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ToolsAndMechanismId",
                table: "ToolCost",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ToolCost_ToolsAndMechanismId",
                table: "ToolCost",
                column: "ToolsAndMechanismId");

            migrationBuilder.AddForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost",
                column: "ToolsAndMechanismId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost");

            migrationBuilder.DropIndex(
                name: "IX_ToolCost_ToolsAndMechanismId",
                table: "ToolCost");

            migrationBuilder.DropColumn(
                name: "ToolsAndMechanismId",
                table: "ToolCost");
        }
    }
}
