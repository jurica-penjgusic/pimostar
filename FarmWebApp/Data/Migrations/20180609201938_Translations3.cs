﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class Translations3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Evidencija_RuralGoods_GospodarskoDobroId",
                table: "Evidencija");

            migrationBuilder.DropForeignKey(
                name: "FK_EvidencijaORadnjama_RuralGoods_GospodarskoDobroId",
                table: "EvidencijaORadnjama");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationTypes_Seasons_SeasonId",
                table: "OperationTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodCosts_RuralCostTypes_RuralCostTypeId",
                table: "RuralGoodCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodCosts_RuralGoods_RuralGoodId",
                table: "RuralGoodCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodGatherings_GatheringCosts_GatheringCostId",
                table: "RuralGoodGatherings");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodGatherings_RuralGoodPerProperties_RuralGoodPerPropertyId",
                table: "RuralGoodGatherings");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodGatherings_Seasons_SeasonId",
                table: "RuralGoodGatherings");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodPerProperties_RuralGoods_RuralGoodId",
                table: "RuralGoodPerProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodPerProperties_Imanja_RuralPropertyId",
                table: "RuralGoodPerProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoods_RuralGoodCategories_RuralCategoryId",
                table: "RuralGoods");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralOperations_OperationActivityTypes_OperationActivityTypeId",
                table: "RuralOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralOperations_OperationTypes_OperationTypeId",
                table: "RuralOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralOperations_Imanja_RuralPropertyId",
                table: "RuralOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolCost_ToolCostType_CostTypeId",
                table: "ToolCost");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsAndMechanisms_Firma_CompanyId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsAndMechanisms_RuralGoodGatherings_RuralGoodGatheringId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_Firma_CompanyId",
                table: "ToolsOutsources");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralOperation_RuralOperations_RuralOperationsId",
                table: "ToolsPerRuralOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralOperation_ToolsAndMechanisms_ToolId",
                table: "ToolsPerRuralOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_AktivnostiNadImanjem_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_Imanja_RuralPropertyId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_ToolsAndMechanisms_ToolId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ToolsPerRuralProperties",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ToolsOutsources",
                table: "ToolsOutsources");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ToolsAndMechanisms",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ToolCostType",
                table: "ToolCostType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ToolCost",
                table: "ToolCost");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralOperations",
                table: "RuralOperations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralGoods",
                table: "RuralGoods");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralGoodPerProperties",
                table: "RuralGoodPerProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralGoodGatherings",
                table: "RuralGoodGatherings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralGoodCosts",
                table: "RuralGoodCosts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralGoodCategories",
                table: "RuralGoodCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralCostTypes",
                table: "RuralCostTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationTypes",
                table: "OperationTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationActivityTypes",
                table: "OperationActivityTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GatheringCosts",
                table: "GatheringCosts");

            migrationBuilder.RenameTable(
                name: "ToolsPerRuralProperties",
                newName: "AlatiIMehanizacijaPoImanju");

            migrationBuilder.RenameTable(
                name: "ToolsOutsources",
                newName: "NajamAlataIMehanizacije");

            migrationBuilder.RenameTable(
                name: "ToolsAndMechanisms",
                newName: "AlatiIMehanizacija");

            migrationBuilder.RenameTable(
                name: "ToolCostType",
                newName: "TipTroskovaAlataIMehanizacije");

            migrationBuilder.RenameTable(
                name: "ToolCost",
                newName: "TroskoviAlataIMehanizacije");

            migrationBuilder.RenameTable(
                name: "RuralOperations",
                newName: "RadnjeNadImanjima");

            migrationBuilder.RenameTable(
                name: "RuralGoods",
                newName: "GospodarskoDobro");

            migrationBuilder.RenameTable(
                name: "RuralGoodPerProperties",
                newName: "GospodarskaDobraPoImanjima");

            migrationBuilder.RenameTable(
                name: "RuralGoodGatherings",
                newName: "Berba");

            migrationBuilder.RenameTable(
                name: "RuralGoodCosts",
                newName: "TroskoviDobara");

            migrationBuilder.RenameTable(
                name: "RuralGoodCategories",
                newName: "KategorijaDobra");

            migrationBuilder.RenameTable(
                name: "RuralCostTypes",
                newName: "TipTroskovaDobara");

            migrationBuilder.RenameTable(
                name: "OperationTypes",
                newName: "TipRadnji");

            migrationBuilder.RenameTable(
                name: "OperationActivityTypes",
                newName: "TipoviAktivnosti");

            migrationBuilder.RenameTable(
                name: "GatheringCosts",
                newName: "TroskoviBerbe");

            migrationBuilder.RenameColumn(
                name: "ToolId",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "AlatIMehanizacijaId");

            migrationBuilder.RenameColumn(
                name: "To",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "VrijemeKraja");

            migrationBuilder.RenameColumn(
                name: "RuralPropertyId",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "ImanjeId");

            migrationBuilder.RenameColumn(
                name: "RuralPropertyActivityId",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "AktivnostId");

            migrationBuilder.RenameColumn(
                name: "From",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "VrijemePocetka");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsPerRuralProperties_ToolId",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "IX_AlatiIMehanizacijaPoImanju_AlatIMehanizacijaId");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsPerRuralProperties_RuralPropertyId",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "IX_AlatiIMehanizacijaPoImanju_ImanjeId");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsPerRuralProperties_RuralPropertyActivityId",
                table: "AlatiIMehanizacijaPoImanju",
                newName: "IX_AlatiIMehanizacijaPoImanju_AktivnostId");

            migrationBuilder.RenameColumn(
                name: "ToolId",
                table: "NajamAlataIMehanizacije",
                newName: "AlatIMehanizacijaId");

            migrationBuilder.RenameColumn(
                name: "To",
                table: "NajamAlataIMehanizacije",
                newName: "KrajNajma");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "NajamAlataIMehanizacije",
                newName: "Cijena");

            migrationBuilder.RenameColumn(
                name: "From",
                table: "NajamAlataIMehanizacije",
                newName: "PocetakNajma");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "NajamAlataIMehanizacije",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Currency",
                table: "NajamAlataIMehanizacije",
                newName: "ValutaPlacanja");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "NajamAlataIMehanizacije",
                newName: "VlasnikId");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsOutsources_ToolId",
                table: "NajamAlataIMehanizacije",
                newName: "IX_NajamAlataIMehanizacije_AlatIMehanizacijaId");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsOutsources_CompanyId",
                table: "NajamAlataIMehanizacije",
                newName: "IX_NajamAlataIMehanizacije_VlasnikId");

            migrationBuilder.RenameColumn(
                name: "UnitMeasurement",
                table: "AlatiIMehanizacija",
                newName: "MjernaJedinicaKolicineNaSkladistu");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "AlatiIMehanizacija",
                newName: "NazivAlata");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "AlatiIMehanizacija",
                newName: "OpisAlata");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "AlatiIMehanizacija",
                newName: "VlasnikId");

            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "AlatiIMehanizacija",
                newName: "KolicinaNaSkladistu");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsAndMechanisms_RuralGoodGatheringId",
                table: "AlatiIMehanizacija",
                newName: "IX_AlatiIMehanizacija_RuralGoodGatheringId");

            migrationBuilder.RenameIndex(
                name: "IX_ToolsAndMechanisms_CompanyId",
                table: "AlatiIMehanizacija",
                newName: "IX_AlatiIMehanizacija_VlasnikId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TipTroskovaAlataIMehanizacije",
                newName: "NazivTipaTroska");

            migrationBuilder.RenameColumn(
                name: "ToolsAndMechanismId",
                table: "TroskoviAlataIMehanizacije",
                newName: "AlatIMehanizacijaId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TroskoviAlataIMehanizacije",
                newName: "NazivTroska");

            migrationBuilder.RenameColumn(
                name: "Currency",
                table: "TroskoviAlataIMehanizacije",
                newName: "ValutaPlacanja");

            migrationBuilder.RenameColumn(
                name: "CostTypeId",
                table: "TroskoviAlataIMehanizacije",
                newName: "TipTroskaId");

            migrationBuilder.RenameColumn(
                name: "CostTime",
                table: "TroskoviAlataIMehanizacije",
                newName: "DatumTroska");

            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "TroskoviAlataIMehanizacije",
                newName: "Iznos");

            migrationBuilder.RenameIndex(
                name: "IX_ToolCost_ToolsAndMechanismId",
                table: "TroskoviAlataIMehanizacije",
                newName: "IX_TroskoviAlataIMehanizacije_AlatIMehanizacijaId");

            migrationBuilder.RenameIndex(
                name: "IX_ToolCost_CostTypeId",
                table: "TroskoviAlataIMehanizacije",
                newName: "IX_TroskoviAlataIMehanizacije_TipTroskaId");

            migrationBuilder.RenameColumn(
                name: "RuralPropertyId",
                table: "RadnjeNadImanjima",
                newName: "ImanjeId");

            migrationBuilder.RenameColumn(
                name: "OperationTypeId",
                table: "RadnjeNadImanjima",
                newName: "TipRadnjeId");

            migrationBuilder.RenameColumn(
                name: "OperationDateTime",
                table: "RadnjeNadImanjima",
                newName: "DatumRadnje");

            migrationBuilder.RenameColumn(
                name: "OperationActivityTypeId",
                table: "RadnjeNadImanjima",
                newName: "AktivnostId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "RadnjeNadImanjima",
                newName: "NazivRadnje");

            migrationBuilder.RenameColumn(
                name: "MeasurementUnit",
                table: "RadnjeNadImanjima",
                newName: "MjernaJedinica");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "RadnjeNadImanjima",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Currency",
                table: "RadnjeNadImanjima",
                newName: "Valuta");

            migrationBuilder.RenameColumn(
                name: "Cost",
                table: "RadnjeNadImanjima",
                newName: "UkupanTrosak");

            migrationBuilder.RenameIndex(
                name: "IX_RuralOperations_RuralPropertyId",
                table: "RadnjeNadImanjima",
                newName: "IX_RadnjeNadImanjima_ImanjeId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralOperations_OperationTypeId",
                table: "RadnjeNadImanjima",
                newName: "IX_RadnjeNadImanjima_TipRadnjeId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralOperations_OperationActivityTypeId",
                table: "RadnjeNadImanjima",
                newName: "IX_RadnjeNadImanjima_AktivnostId");

            migrationBuilder.RenameColumn(
                name: "RuralCategoryId",
                table: "GospodarskoDobro",
                newName: "KategorijaDobraId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "GospodarskoDobro",
                newName: "NazivGosporaskogDobra");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "GospodarskoDobro",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Active",
                table: "GospodarskoDobro",
                newName: "Zivo");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoods_RuralCategoryId",
                table: "GospodarskoDobro",
                newName: "IX_GospodarskoDobro_KategorijaDobraId");

            migrationBuilder.RenameColumn(
                name: "TimeOfProductPerLand",
                table: "GospodarskaDobraPoImanjima",
                newName: "VremenskiPeriod");

            migrationBuilder.RenameColumn(
                name: "RuralPropertyId",
                table: "GospodarskaDobraPoImanjima",
                newName: "ImanjeId");

            migrationBuilder.RenameColumn(
                name: "RuralGoodId",
                table: "GospodarskaDobraPoImanjima",
                newName: "GospodarskoDobroId");

            migrationBuilder.RenameColumn(
                name: "MeasurementUnit",
                table: "GospodarskaDobraPoImanjima",
                newName: "MjernaJedinica");

            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "GospodarskaDobraPoImanjima",
                newName: "Kolicina");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodPerProperties_RuralPropertyId",
                table: "GospodarskaDobraPoImanjima",
                newName: "IX_GospodarskaDobraPoImanjima_ImanjeId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodPerProperties_RuralGoodId",
                table: "GospodarskaDobraPoImanjima",
                newName: "IX_GospodarskaDobraPoImanjima_GospodarskoDobroId");

            migrationBuilder.RenameColumn(
                name: "SeasonId",
                table: "Berba",
                newName: "SezonaId");

            migrationBuilder.RenameColumn(
                name: "RuralGoodPerPropertyId",
                table: "Berba",
                newName: "ImanjeId");

            migrationBuilder.RenameColumn(
                name: "MeasurementUnit",
                table: "Berba",
                newName: "MjernaJedinica");

            migrationBuilder.RenameColumn(
                name: "GatheringTime",
                table: "Berba",
                newName: "DatumeBerbe");

            migrationBuilder.RenameColumn(
                name: "GatheringCostId",
                table: "Berba",
                newName: "TrosakId");

            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "Berba",
                newName: "Kolicina");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodGatherings_SeasonId",
                table: "Berba",
                newName: "IX_Berba_SezonaId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodGatherings_RuralGoodPerPropertyId",
                table: "Berba",
                newName: "IX_Berba_ImanjeId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodGatherings_GatheringCostId",
                table: "Berba",
                newName: "IX_Berba_TrosakId");

            migrationBuilder.RenameColumn(
                name: "RuralCostTypeId",
                table: "TroskoviDobara",
                newName: "TipTroskaId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TroskoviDobara",
                newName: "NazivTroska");

            migrationBuilder.RenameColumn(
                name: "Currency",
                table: "TroskoviDobara",
                newName: "Valuta");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "TroskoviDobara",
                newName: "DatumTroska");

            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "TroskoviDobara",
                newName: "Iznos");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodCosts_RuralGoodId",
                table: "TroskoviDobara",
                newName: "IX_TroskoviDobara_RuralGoodId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralGoodCosts_RuralCostTypeId",
                table: "TroskoviDobara",
                newName: "IX_TroskoviDobara_TipTroskaId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "KategorijaDobra",
                newName: "NazivKategorije");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "KategorijaDobra",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TipTroskovaDobara",
                newName: "NazivTipaTroska");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "TipTroskovaDobara",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "SeasonId",
                table: "TipRadnji",
                newName: "SezonaId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TipRadnji",
                newName: "NazivRadnje");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "TipRadnji",
                newName: "Opis");

            migrationBuilder.RenameIndex(
                name: "IX_OperationTypes_SeasonId",
                table: "TipRadnji",
                newName: "IX_TipRadnji_SezonaId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TipoviAktivnosti",
                newName: "NazivTipaAktivnosti");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "TipoviAktivnosti",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TroskoviBerbe",
                newName: "NazivTroska");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "TroskoviBerbe",
                newName: "Opis");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlatiIMehanizacijaPoImanju",
                table: "AlatiIMehanizacijaPoImanju",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NajamAlataIMehanizacije",
                table: "NajamAlataIMehanizacije",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlatiIMehanizacija",
                table: "AlatiIMehanizacija",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipTroskovaAlataIMehanizacije",
                table: "TipTroskovaAlataIMehanizacije",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TroskoviAlataIMehanizacije",
                table: "TroskoviAlataIMehanizacije",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RadnjeNadImanjima",
                table: "RadnjeNadImanjima",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GospodarskoDobro",
                table: "GospodarskoDobro",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GospodarskaDobraPoImanjima",
                table: "GospodarskaDobraPoImanjima",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Berba",
                table: "Berba",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TroskoviDobara",
                table: "TroskoviDobara",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_KategorijaDobra",
                table: "KategorijaDobra",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipTroskovaDobara",
                table: "TipTroskovaDobara",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipRadnji",
                table: "TipRadnji",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipoviAktivnosti",
                table: "TipoviAktivnosti",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TroskoviBerbe",
                table: "TroskoviBerbe",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AlatiIMehanizacija_Firma_VlasnikId",
                table: "AlatiIMehanizacija",
                column: "VlasnikId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AlatiIMehanizacija_Berba_RuralGoodGatheringId",
                table: "AlatiIMehanizacija",
                column: "RuralGoodGatheringId",
                principalTable: "Berba",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AlatiIMehanizacijaPoImanju_AktivnostiNadImanjem_AktivnostId",
                table: "AlatiIMehanizacijaPoImanju",
                column: "AktivnostId",
                principalTable: "AktivnostiNadImanjem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AlatiIMehanizacijaPoImanju_Imanja_ImanjeId",
                table: "AlatiIMehanizacijaPoImanju",
                column: "ImanjeId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AlatiIMehanizacijaPoImanju_AlatiIMehanizacija_AlatIMehanizacijaId",
                table: "AlatiIMehanizacijaPoImanju",
                column: "AlatIMehanizacijaId",
                principalTable: "AlatiIMehanizacija",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Berba_TroskoviBerbe_TrosakId",
                table: "Berba",
                column: "TrosakId",
                principalTable: "TroskoviBerbe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Berba_GospodarskaDobraPoImanjima_ImanjeId",
                table: "Berba",
                column: "ImanjeId",
                principalTable: "GospodarskaDobraPoImanjima",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Berba_Seasons_SezonaId",
                table: "Berba",
                column: "SezonaId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Evidencija_GospodarskoDobro_GospodarskoDobroId",
                table: "Evidencija",
                column: "GospodarskoDobroId",
                principalTable: "GospodarskoDobro",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EvidencijaORadnjama_GospodarskoDobro_GospodarskoDobroId",
                table: "EvidencijaORadnjama",
                column: "GospodarskoDobroId",
                principalTable: "GospodarskoDobro",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GospodarskaDobraPoImanjima_GospodarskoDobro_GospodarskoDobroId",
                table: "GospodarskaDobraPoImanjima",
                column: "GospodarskoDobroId",
                principalTable: "GospodarskoDobro",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GospodarskaDobraPoImanjima_Imanja_ImanjeId",
                table: "GospodarskaDobraPoImanjima",
                column: "ImanjeId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GospodarskoDobro_KategorijaDobra_KategorijaDobraId",
                table: "GospodarskoDobro",
                column: "KategorijaDobraId",
                principalTable: "KategorijaDobra",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NajamAlataIMehanizacije_Firma_VlasnikId",
                table: "NajamAlataIMehanizacije",
                column: "VlasnikId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NajamAlataIMehanizacije_AlatiIMehanizacija_AlatIMehanizacijaId",
                table: "NajamAlataIMehanizacije",
                column: "AlatIMehanizacijaId",
                principalTable: "AlatiIMehanizacija",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RadnjeNadImanjima_TipoviAktivnosti_AktivnostId",
                table: "RadnjeNadImanjima",
                column: "AktivnostId",
                principalTable: "TipoviAktivnosti",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RadnjeNadImanjima_TipRadnji_TipRadnjeId",
                table: "RadnjeNadImanjima",
                column: "TipRadnjeId",
                principalTable: "TipRadnji",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RadnjeNadImanjima_Imanja_ImanjeId",
                table: "RadnjeNadImanjima",
                column: "ImanjeId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TipRadnji_Seasons_SezonaId",
                table: "TipRadnji",
                column: "SezonaId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralOperation_RadnjeNadImanjima_RuralOperationsId",
                table: "ToolsPerRuralOperation",
                column: "RuralOperationsId",
                principalTable: "RadnjeNadImanjima",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralOperation_AlatiIMehanizacija_ToolId",
                table: "ToolsPerRuralOperation",
                column: "ToolId",
                principalTable: "AlatiIMehanizacija",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TroskoviAlataIMehanizacije_TipTroskovaAlataIMehanizacije_TipTroskaId",
                table: "TroskoviAlataIMehanizacije",
                column: "TipTroskaId",
                principalTable: "TipTroskovaAlataIMehanizacije",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TroskoviAlataIMehanizacije_AlatiIMehanizacija_AlatIMehanizacijaId",
                table: "TroskoviAlataIMehanizacije",
                column: "AlatIMehanizacijaId",
                principalTable: "AlatiIMehanizacija",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TroskoviDobara_TipTroskovaDobara_TipTroskaId",
                table: "TroskoviDobara",
                column: "TipTroskaId",
                principalTable: "TipTroskovaDobara",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TroskoviDobara_GospodarskoDobro_RuralGoodId",
                table: "TroskoviDobara",
                column: "RuralGoodId",
                principalTable: "GospodarskoDobro",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlatiIMehanizacija_Firma_VlasnikId",
                table: "AlatiIMehanizacija");

            migrationBuilder.DropForeignKey(
                name: "FK_AlatiIMehanizacija_Berba_RuralGoodGatheringId",
                table: "AlatiIMehanizacija");

            migrationBuilder.DropForeignKey(
                name: "FK_AlatiIMehanizacijaPoImanju_AktivnostiNadImanjem_AktivnostId",
                table: "AlatiIMehanizacijaPoImanju");

            migrationBuilder.DropForeignKey(
                name: "FK_AlatiIMehanizacijaPoImanju_Imanja_ImanjeId",
                table: "AlatiIMehanizacijaPoImanju");

            migrationBuilder.DropForeignKey(
                name: "FK_AlatiIMehanizacijaPoImanju_AlatiIMehanizacija_AlatIMehanizacijaId",
                table: "AlatiIMehanizacijaPoImanju");

            migrationBuilder.DropForeignKey(
                name: "FK_Berba_TroskoviBerbe_TrosakId",
                table: "Berba");

            migrationBuilder.DropForeignKey(
                name: "FK_Berba_GospodarskaDobraPoImanjima_ImanjeId",
                table: "Berba");

            migrationBuilder.DropForeignKey(
                name: "FK_Berba_Seasons_SezonaId",
                table: "Berba");

            migrationBuilder.DropForeignKey(
                name: "FK_Evidencija_GospodarskoDobro_GospodarskoDobroId",
                table: "Evidencija");

            migrationBuilder.DropForeignKey(
                name: "FK_EvidencijaORadnjama_GospodarskoDobro_GospodarskoDobroId",
                table: "EvidencijaORadnjama");

            migrationBuilder.DropForeignKey(
                name: "FK_GospodarskaDobraPoImanjima_GospodarskoDobro_GospodarskoDobroId",
                table: "GospodarskaDobraPoImanjima");

            migrationBuilder.DropForeignKey(
                name: "FK_GospodarskaDobraPoImanjima_Imanja_ImanjeId",
                table: "GospodarskaDobraPoImanjima");

            migrationBuilder.DropForeignKey(
                name: "FK_GospodarskoDobro_KategorijaDobra_KategorijaDobraId",
                table: "GospodarskoDobro");

            migrationBuilder.DropForeignKey(
                name: "FK_NajamAlataIMehanizacije_Firma_VlasnikId",
                table: "NajamAlataIMehanizacije");

            migrationBuilder.DropForeignKey(
                name: "FK_NajamAlataIMehanizacije_AlatiIMehanizacija_AlatIMehanizacijaId",
                table: "NajamAlataIMehanizacije");

            migrationBuilder.DropForeignKey(
                name: "FK_RadnjeNadImanjima_TipoviAktivnosti_AktivnostId",
                table: "RadnjeNadImanjima");

            migrationBuilder.DropForeignKey(
                name: "FK_RadnjeNadImanjima_TipRadnji_TipRadnjeId",
                table: "RadnjeNadImanjima");

            migrationBuilder.DropForeignKey(
                name: "FK_RadnjeNadImanjima_Imanja_ImanjeId",
                table: "RadnjeNadImanjima");

            migrationBuilder.DropForeignKey(
                name: "FK_TipRadnji_Seasons_SezonaId",
                table: "TipRadnji");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralOperation_RadnjeNadImanjima_RuralOperationsId",
                table: "ToolsPerRuralOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralOperation_AlatiIMehanizacija_ToolId",
                table: "ToolsPerRuralOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_TroskoviAlataIMehanizacije_TipTroskovaAlataIMehanizacije_TipTroskaId",
                table: "TroskoviAlataIMehanizacije");

            migrationBuilder.DropForeignKey(
                name: "FK_TroskoviAlataIMehanizacije_AlatiIMehanizacija_AlatIMehanizacijaId",
                table: "TroskoviAlataIMehanizacije");

            migrationBuilder.DropForeignKey(
                name: "FK_TroskoviDobara_TipTroskovaDobara_TipTroskaId",
                table: "TroskoviDobara");

            migrationBuilder.DropForeignKey(
                name: "FK_TroskoviDobara_GospodarskoDobro_RuralGoodId",
                table: "TroskoviDobara");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TroskoviDobara",
                table: "TroskoviDobara");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TroskoviBerbe",
                table: "TroskoviBerbe");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TroskoviAlataIMehanizacije",
                table: "TroskoviAlataIMehanizacije");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipTroskovaDobara",
                table: "TipTroskovaDobara");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipTroskovaAlataIMehanizacije",
                table: "TipTroskovaAlataIMehanizacije");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipRadnji",
                table: "TipRadnji");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipoviAktivnosti",
                table: "TipoviAktivnosti");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RadnjeNadImanjima",
                table: "RadnjeNadImanjima");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NajamAlataIMehanizacije",
                table: "NajamAlataIMehanizacije");

            migrationBuilder.DropPrimaryKey(
                name: "PK_KategorijaDobra",
                table: "KategorijaDobra");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GospodarskoDobro",
                table: "GospodarskoDobro");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GospodarskaDobraPoImanjima",
                table: "GospodarskaDobraPoImanjima");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Berba",
                table: "Berba");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlatiIMehanizacijaPoImanju",
                table: "AlatiIMehanizacijaPoImanju");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlatiIMehanizacija",
                table: "AlatiIMehanizacija");

            migrationBuilder.RenameTable(
                name: "TroskoviDobara",
                newName: "RuralGoodCosts");

            migrationBuilder.RenameTable(
                name: "TroskoviBerbe",
                newName: "GatheringCosts");

            migrationBuilder.RenameTable(
                name: "TroskoviAlataIMehanizacije",
                newName: "ToolCost");

            migrationBuilder.RenameTable(
                name: "TipTroskovaDobara",
                newName: "RuralCostTypes");

            migrationBuilder.RenameTable(
                name: "TipTroskovaAlataIMehanizacije",
                newName: "ToolCostType");

            migrationBuilder.RenameTable(
                name: "TipRadnji",
                newName: "OperationTypes");

            migrationBuilder.RenameTable(
                name: "TipoviAktivnosti",
                newName: "OperationActivityTypes");

            migrationBuilder.RenameTable(
                name: "RadnjeNadImanjima",
                newName: "RuralOperations");

            migrationBuilder.RenameTable(
                name: "NajamAlataIMehanizacije",
                newName: "ToolsOutsources");

            migrationBuilder.RenameTable(
                name: "KategorijaDobra",
                newName: "RuralGoodCategories");

            migrationBuilder.RenameTable(
                name: "GospodarskoDobro",
                newName: "RuralGoods");

            migrationBuilder.RenameTable(
                name: "GospodarskaDobraPoImanjima",
                newName: "RuralGoodPerProperties");

            migrationBuilder.RenameTable(
                name: "Berba",
                newName: "RuralGoodGatherings");

            migrationBuilder.RenameTable(
                name: "AlatiIMehanizacijaPoImanju",
                newName: "ToolsPerRuralProperties");

            migrationBuilder.RenameTable(
                name: "AlatiIMehanizacija",
                newName: "ToolsAndMechanisms");

            migrationBuilder.RenameColumn(
                name: "TipTroskaId",
                table: "RuralGoodCosts",
                newName: "RuralCostTypeId");

            migrationBuilder.RenameColumn(
                name: "NazivTroska",
                table: "RuralGoodCosts",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Valuta",
                table: "RuralGoodCosts",
                newName: "Currency");

            migrationBuilder.RenameColumn(
                name: "DatumTroska",
                table: "RuralGoodCosts",
                newName: "Created");

            migrationBuilder.RenameColumn(
                name: "Iznos",
                table: "RuralGoodCosts",
                newName: "Amount");

            migrationBuilder.RenameIndex(
                name: "IX_TroskoviDobara_RuralGoodId",
                table: "RuralGoodCosts",
                newName: "IX_RuralGoodCosts_RuralGoodId");

            migrationBuilder.RenameIndex(
                name: "IX_TroskoviDobara_TipTroskaId",
                table: "RuralGoodCosts",
                newName: "IX_RuralGoodCosts_RuralCostTypeId");

            migrationBuilder.RenameColumn(
                name: "NazivTroska",
                table: "GatheringCosts",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "GatheringCosts",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "AlatIMehanizacijaId",
                table: "ToolCost",
                newName: "ToolsAndMechanismId");

            migrationBuilder.RenameColumn(
                name: "NazivTroska",
                table: "ToolCost",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ValutaPlacanja",
                table: "ToolCost",
                newName: "Currency");

            migrationBuilder.RenameColumn(
                name: "TipTroskaId",
                table: "ToolCost",
                newName: "CostTypeId");

            migrationBuilder.RenameColumn(
                name: "DatumTroska",
                table: "ToolCost",
                newName: "CostTime");

            migrationBuilder.RenameColumn(
                name: "Iznos",
                table: "ToolCost",
                newName: "Amount");

            migrationBuilder.RenameIndex(
                name: "IX_TroskoviAlataIMehanizacije_AlatIMehanizacijaId",
                table: "ToolCost",
                newName: "IX_ToolCost_ToolsAndMechanismId");

            migrationBuilder.RenameIndex(
                name: "IX_TroskoviAlataIMehanizacije_TipTroskaId",
                table: "ToolCost",
                newName: "IX_ToolCost_CostTypeId");

            migrationBuilder.RenameColumn(
                name: "NazivTipaTroska",
                table: "RuralCostTypes",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralCostTypes",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "NazivTipaTroska",
                table: "ToolCostType",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "SezonaId",
                table: "OperationTypes",
                newName: "SeasonId");

            migrationBuilder.RenameColumn(
                name: "NazivRadnje",
                table: "OperationTypes",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "OperationTypes",
                newName: "Description");

            migrationBuilder.RenameIndex(
                name: "IX_TipRadnji_SezonaId",
                table: "OperationTypes",
                newName: "IX_OperationTypes_SeasonId");

            migrationBuilder.RenameColumn(
                name: "NazivTipaAktivnosti",
                table: "OperationActivityTypes",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "OperationActivityTypes",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "ImanjeId",
                table: "RuralOperations",
                newName: "RuralPropertyId");

            migrationBuilder.RenameColumn(
                name: "TipRadnjeId",
                table: "RuralOperations",
                newName: "OperationTypeId");

            migrationBuilder.RenameColumn(
                name: "DatumRadnje",
                table: "RuralOperations",
                newName: "OperationDateTime");

            migrationBuilder.RenameColumn(
                name: "AktivnostId",
                table: "RuralOperations",
                newName: "OperationActivityTypeId");

            migrationBuilder.RenameColumn(
                name: "NazivRadnje",
                table: "RuralOperations",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "MjernaJedinica",
                table: "RuralOperations",
                newName: "MeasurementUnit");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralOperations",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "Valuta",
                table: "RuralOperations",
                newName: "Currency");

            migrationBuilder.RenameColumn(
                name: "UkupanTrosak",
                table: "RuralOperations",
                newName: "Cost");

            migrationBuilder.RenameIndex(
                name: "IX_RadnjeNadImanjima_ImanjeId",
                table: "RuralOperations",
                newName: "IX_RuralOperations_RuralPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_RadnjeNadImanjima_TipRadnjeId",
                table: "RuralOperations",
                newName: "IX_RuralOperations_OperationTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_RadnjeNadImanjima_AktivnostId",
                table: "RuralOperations",
                newName: "IX_RuralOperations_OperationActivityTypeId");

            migrationBuilder.RenameColumn(
                name: "AlatIMehanizacijaId",
                table: "ToolsOutsources",
                newName: "ToolId");

            migrationBuilder.RenameColumn(
                name: "KrajNajma",
                table: "ToolsOutsources",
                newName: "To");

            migrationBuilder.RenameColumn(
                name: "Cijena",
                table: "ToolsOutsources",
                newName: "Price");

            migrationBuilder.RenameColumn(
                name: "PocetakNajma",
                table: "ToolsOutsources",
                newName: "From");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "ToolsOutsources",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "ValutaPlacanja",
                table: "ToolsOutsources",
                newName: "Currency");

            migrationBuilder.RenameColumn(
                name: "VlasnikId",
                table: "ToolsOutsources",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_NajamAlataIMehanizacije_AlatIMehanizacijaId",
                table: "ToolsOutsources",
                newName: "IX_ToolsOutsources_ToolId");

            migrationBuilder.RenameIndex(
                name: "IX_NajamAlataIMehanizacije_VlasnikId",
                table: "ToolsOutsources",
                newName: "IX_ToolsOutsources_CompanyId");

            migrationBuilder.RenameColumn(
                name: "NazivKategorije",
                table: "RuralGoodCategories",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralGoodCategories",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "KategorijaDobraId",
                table: "RuralGoods",
                newName: "RuralCategoryId");

            migrationBuilder.RenameColumn(
                name: "NazivGosporaskogDobra",
                table: "RuralGoods",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralGoods",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "Zivo",
                table: "RuralGoods",
                newName: "Active");

            migrationBuilder.RenameIndex(
                name: "IX_GospodarskoDobro_KategorijaDobraId",
                table: "RuralGoods",
                newName: "IX_RuralGoods_RuralCategoryId");

            migrationBuilder.RenameColumn(
                name: "VremenskiPeriod",
                table: "RuralGoodPerProperties",
                newName: "TimeOfProductPerLand");

            migrationBuilder.RenameColumn(
                name: "ImanjeId",
                table: "RuralGoodPerProperties",
                newName: "RuralPropertyId");

            migrationBuilder.RenameColumn(
                name: "GospodarskoDobroId",
                table: "RuralGoodPerProperties",
                newName: "RuralGoodId");

            migrationBuilder.RenameColumn(
                name: "MjernaJedinica",
                table: "RuralGoodPerProperties",
                newName: "MeasurementUnit");

            migrationBuilder.RenameColumn(
                name: "Kolicina",
                table: "RuralGoodPerProperties",
                newName: "Amount");

            migrationBuilder.RenameIndex(
                name: "IX_GospodarskaDobraPoImanjima_ImanjeId",
                table: "RuralGoodPerProperties",
                newName: "IX_RuralGoodPerProperties_RuralPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_GospodarskaDobraPoImanjima_GospodarskoDobroId",
                table: "RuralGoodPerProperties",
                newName: "IX_RuralGoodPerProperties_RuralGoodId");

            migrationBuilder.RenameColumn(
                name: "SezonaId",
                table: "RuralGoodGatherings",
                newName: "SeasonId");

            migrationBuilder.RenameColumn(
                name: "ImanjeId",
                table: "RuralGoodGatherings",
                newName: "RuralGoodPerPropertyId");

            migrationBuilder.RenameColumn(
                name: "MjernaJedinica",
                table: "RuralGoodGatherings",
                newName: "MeasurementUnit");

            migrationBuilder.RenameColumn(
                name: "DatumeBerbe",
                table: "RuralGoodGatherings",
                newName: "GatheringTime");

            migrationBuilder.RenameColumn(
                name: "TrosakId",
                table: "RuralGoodGatherings",
                newName: "GatheringCostId");

            migrationBuilder.RenameColumn(
                name: "Kolicina",
                table: "RuralGoodGatherings",
                newName: "Amount");

            migrationBuilder.RenameIndex(
                name: "IX_Berba_SezonaId",
                table: "RuralGoodGatherings",
                newName: "IX_RuralGoodGatherings_SeasonId");

            migrationBuilder.RenameIndex(
                name: "IX_Berba_ImanjeId",
                table: "RuralGoodGatherings",
                newName: "IX_RuralGoodGatherings_RuralGoodPerPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_Berba_TrosakId",
                table: "RuralGoodGatherings",
                newName: "IX_RuralGoodGatherings_GatheringCostId");

            migrationBuilder.RenameColumn(
                name: "AlatIMehanizacijaId",
                table: "ToolsPerRuralProperties",
                newName: "ToolId");

            migrationBuilder.RenameColumn(
                name: "VrijemeKraja",
                table: "ToolsPerRuralProperties",
                newName: "To");

            migrationBuilder.RenameColumn(
                name: "ImanjeId",
                table: "ToolsPerRuralProperties",
                newName: "RuralPropertyId");

            migrationBuilder.RenameColumn(
                name: "AktivnostId",
                table: "ToolsPerRuralProperties",
                newName: "RuralPropertyActivityId");

            migrationBuilder.RenameColumn(
                name: "VrijemePocetka",
                table: "ToolsPerRuralProperties",
                newName: "From");

            migrationBuilder.RenameIndex(
                name: "IX_AlatiIMehanizacijaPoImanju_AlatIMehanizacijaId",
                table: "ToolsPerRuralProperties",
                newName: "IX_ToolsPerRuralProperties_ToolId");

            migrationBuilder.RenameIndex(
                name: "IX_AlatiIMehanizacijaPoImanju_ImanjeId",
                table: "ToolsPerRuralProperties",
                newName: "IX_ToolsPerRuralProperties_RuralPropertyId");

            migrationBuilder.RenameIndex(
                name: "IX_AlatiIMehanizacijaPoImanju_AktivnostId",
                table: "ToolsPerRuralProperties",
                newName: "IX_ToolsPerRuralProperties_RuralPropertyActivityId");

            migrationBuilder.RenameColumn(
                name: "MjernaJedinicaKolicineNaSkladistu",
                table: "ToolsAndMechanisms",
                newName: "UnitMeasurement");

            migrationBuilder.RenameColumn(
                name: "NazivAlata",
                table: "ToolsAndMechanisms",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "OpisAlata",
                table: "ToolsAndMechanisms",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "VlasnikId",
                table: "ToolsAndMechanisms",
                newName: "CompanyId");

            migrationBuilder.RenameColumn(
                name: "KolicinaNaSkladistu",
                table: "ToolsAndMechanisms",
                newName: "Amount");

            migrationBuilder.RenameIndex(
                name: "IX_AlatiIMehanizacija_RuralGoodGatheringId",
                table: "ToolsAndMechanisms",
                newName: "IX_ToolsAndMechanisms_RuralGoodGatheringId");

            migrationBuilder.RenameIndex(
                name: "IX_AlatiIMehanizacija_VlasnikId",
                table: "ToolsAndMechanisms",
                newName: "IX_ToolsAndMechanisms_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralGoodCosts",
                table: "RuralGoodCosts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GatheringCosts",
                table: "GatheringCosts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ToolCost",
                table: "ToolCost",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralCostTypes",
                table: "RuralCostTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ToolCostType",
                table: "ToolCostType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationTypes",
                table: "OperationTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationActivityTypes",
                table: "OperationActivityTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralOperations",
                table: "RuralOperations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ToolsOutsources",
                table: "ToolsOutsources",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralGoodCategories",
                table: "RuralGoodCategories",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralGoods",
                table: "RuralGoods",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralGoodPerProperties",
                table: "RuralGoodPerProperties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralGoodGatherings",
                table: "RuralGoodGatherings",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ToolsPerRuralProperties",
                table: "ToolsPerRuralProperties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ToolsAndMechanisms",
                table: "ToolsAndMechanisms",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Evidencija_RuralGoods_GospodarskoDobroId",
                table: "Evidencija",
                column: "GospodarskoDobroId",
                principalTable: "RuralGoods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EvidencijaORadnjama_RuralGoods_GospodarskoDobroId",
                table: "EvidencijaORadnjama",
                column: "GospodarskoDobroId",
                principalTable: "RuralGoods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationTypes_Seasons_SeasonId",
                table: "OperationTypes",
                column: "SeasonId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodCosts_RuralCostTypes_RuralCostTypeId",
                table: "RuralGoodCosts",
                column: "RuralCostTypeId",
                principalTable: "RuralCostTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodCosts_RuralGoods_RuralGoodId",
                table: "RuralGoodCosts",
                column: "RuralGoodId",
                principalTable: "RuralGoods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodGatherings_GatheringCosts_GatheringCostId",
                table: "RuralGoodGatherings",
                column: "GatheringCostId",
                principalTable: "GatheringCosts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodGatherings_RuralGoodPerProperties_RuralGoodPerPropertyId",
                table: "RuralGoodGatherings",
                column: "RuralGoodPerPropertyId",
                principalTable: "RuralGoodPerProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodGatherings_Seasons_SeasonId",
                table: "RuralGoodGatherings",
                column: "SeasonId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodPerProperties_RuralGoods_RuralGoodId",
                table: "RuralGoodPerProperties",
                column: "RuralGoodId",
                principalTable: "RuralGoods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodPerProperties_Imanja_RuralPropertyId",
                table: "RuralGoodPerProperties",
                column: "RuralPropertyId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoods_RuralGoodCategories_RuralCategoryId",
                table: "RuralGoods",
                column: "RuralCategoryId",
                principalTable: "RuralGoodCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralOperations_OperationActivityTypes_OperationActivityTypeId",
                table: "RuralOperations",
                column: "OperationActivityTypeId",
                principalTable: "OperationActivityTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralOperations_OperationTypes_OperationTypeId",
                table: "RuralOperations",
                column: "OperationTypeId",
                principalTable: "OperationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralOperations_Imanja_RuralPropertyId",
                table: "RuralOperations",
                column: "RuralPropertyId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolCost_ToolCostType_CostTypeId",
                table: "ToolCost",
                column: "CostTypeId",
                principalTable: "ToolCostType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolCost_ToolsAndMechanisms_ToolsAndMechanismId",
                table: "ToolCost",
                column: "ToolsAndMechanismId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsAndMechanisms_Firma_CompanyId",
                table: "ToolsAndMechanisms",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsAndMechanisms_RuralGoodGatherings_RuralGoodGatheringId",
                table: "ToolsAndMechanisms",
                column: "RuralGoodGatheringId",
                principalTable: "RuralGoodGatherings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_Firma_CompanyId",
                table: "ToolsOutsources",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralOperation_RuralOperations_RuralOperationsId",
                table: "ToolsPerRuralOperation",
                column: "RuralOperationsId",
                principalTable: "RuralOperations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralOperation_ToolsAndMechanisms_ToolId",
                table: "ToolsPerRuralOperation",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_AktivnostiNadImanjem_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyActivityId",
                principalTable: "AktivnostiNadImanjem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_Imanja_RuralPropertyId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_ToolsAndMechanisms_ToolId",
                table: "ToolsPerRuralProperties",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
