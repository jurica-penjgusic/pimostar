﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class AddRelationshipToCompanySalePlaces : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "CompanySalePlaces",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CompanySalePlaces_CompanyId",
                table: "CompanySalePlaces",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanySalePlaces_Companies_CompanyId",
                table: "CompanySalePlaces",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanySalePlaces_Companies_CompanyId",
                table: "CompanySalePlaces");

            migrationBuilder.DropIndex(
                name: "IX_CompanySalePlaces_CompanyId",
                table: "CompanySalePlaces");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "CompanySalePlaces");
        }
    }
}
