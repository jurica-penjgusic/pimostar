﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class ToolsForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.AlterColumn<int>(
                name: "RuralPropertyId",
                table: "ToolsPerRuralProperties",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "ToolId",
                table: "ToolsPerRuralProperties",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ToolsPerRuralProperties_ToolId",
                table: "ToolsPerRuralProperties",
                column: "ToolId");

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyId",
                principalTable: "RuralProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_ToolsAndMechanisms_ToolId",
                table: "ToolsPerRuralProperties",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_ToolsAndMechanisms_ToolId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropIndex(
                name: "IX_ToolsPerRuralProperties_ToolId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropColumn(
                name: "ToolId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.AlterColumn<int>(
                name: "RuralPropertyId",
                table: "ToolsPerRuralProperties",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyId",
                principalTable: "RuralProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
