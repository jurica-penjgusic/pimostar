﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class TrackerAndMilkMatName3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TipAktivnosti",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(nullable: true),
                    Opis = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipAktivnosti", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EvidencijaORadnjama",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cijena = table.Column<decimal>(nullable: false),
                    DatumIzvrsenja = table.Column<DateTime>(nullable: false),
                    GospodarskoDobroId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    TipAktivnostiId = table.Column<int>(nullable: true),
                    Valuta = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvidencijaORadnjama", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvidencijaORadnjama_RuralGoods_GospodarskoDobroId",
                        column: x => x.GospodarskoDobroId,
                        principalTable: "RuralGoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvidencijaORadnjama_TipAktivnosti_TipAktivnostiId",
                        column: x => x.TipAktivnostiId,
                        principalTable: "TipAktivnosti",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EvidencijaORadnjama_GospodarskoDobroId",
                table: "EvidencijaORadnjama",
                column: "GospodarskoDobroId");

            migrationBuilder.CreateIndex(
                name: "IX_EvidencijaORadnjama_TipAktivnostiId",
                table: "EvidencijaORadnjama",
                column: "TipAktivnostiId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EvidencijaORadnjama");

            migrationBuilder.DropTable(
                name: "TipAktivnosti");
        }
    }
}
