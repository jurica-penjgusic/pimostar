﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class Translations2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanySalePlaces_Firma_CompanyId",
                table: "CompanySalePlaces");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanySalePlaces_SalePlaceCatogories_SalePlaceCategoryId",
                table: "CompanySalePlaces");

            migrationBuilder.DropForeignKey(
                name: "FK_Firma_CompanyTypes_TipFirme",
                table: "Firma");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodPerProperties_RuralProperties_RuralPropertyId",
                table: "RuralGoodPerProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralOperations_RuralProperties_RuralPropertyId",
                table: "RuralOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralProperties_Firma_CompanyId",
                table: "RuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralProperties_RuralPropertyTypes_RuralPropertyTypeId",
                table: "RuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralPropertyActivities_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SalePlaceCatogories",
                table: "SalePlaceCatogories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralPropertyTypes",
                table: "RuralPropertyTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralPropertyActivities",
                table: "RuralPropertyActivities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RuralProperties",
                table: "RuralProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyTypes",
                table: "CompanyTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanySalePlaces",
                table: "CompanySalePlaces");

            migrationBuilder.RenameTable(
                name: "SalePlaceCatogories",
                newName: "KategorijaProdajnihMjesta");

            migrationBuilder.RenameTable(
                name: "RuralPropertyTypes",
                newName: "TipImanja");

            migrationBuilder.RenameTable(
                name: "RuralPropertyActivities",
                newName: "AktivnostiNadImanjem");

            migrationBuilder.RenameTable(
                name: "RuralProperties",
                newName: "Imanja");

            migrationBuilder.RenameTable(
                name: "CompanyTypes",
                newName: "TipFirme");

            migrationBuilder.RenameTable(
                name: "CompanySalePlaces",
                newName: "ProdajnaMjestaFirme");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "KategorijaProdajnihMjesta",
                newName: "NazivKategorijeProdajnogMjesta");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TipImanja",
                newName: "NazivTipaImanja");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "TipImanja",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "AktivnostiNadImanjem",
                newName: "Naziv");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "AktivnostiNadImanjem",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "SizeMeasurementUnit",
                table: "Imanja",
                newName: "MjernaJedinica");

            migrationBuilder.RenameColumn(
                name: "Size",
                table: "Imanja",
                newName: "Velicina");

            migrationBuilder.RenameColumn(
                name: "RuralPropertyTypeId",
                table: "Imanja",
                newName: "TipImanjaId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Imanja",
                newName: "Naziv");

            migrationBuilder.RenameColumn(
                name: "Longitute",
                table: "Imanja",
                newName: "GeografskaDuzina");

            migrationBuilder.RenameColumn(
                name: "LocalIdentifier",
                table: "Imanja",
                newName: "LokalniNaziv");

            migrationBuilder.RenameColumn(
                name: "Latitude",
                table: "Imanja",
                newName: "GeografskaSirina");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Imanja",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "Imanja",
                newName: "VlasnikId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralProperties_RuralPropertyTypeId",
                table: "Imanja",
                newName: "IX_Imanja_TipImanjaId");

            migrationBuilder.RenameIndex(
                name: "IX_RuralProperties_CompanyId",
                table: "Imanja",
                newName: "IX_Imanja_VlasnikId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "TipFirme",
                newName: "NazivTipaFirme");

            migrationBuilder.RenameColumn(
                name: "SalePlaceName",
                table: "ProdajnaMjestaFirme",
                newName: "Naziv");

            migrationBuilder.RenameColumn(
                name: "SalePlaceCategoryId",
                table: "ProdajnaMjestaFirme",
                newName: "KategorijaProdajnogMjestaId");

            migrationBuilder.RenameColumn(
                name: "Longitude",
                table: "ProdajnaMjestaFirme",
                newName: "GeografskaDuzina");

            migrationBuilder.RenameColumn(
                name: "Latitude",
                table: "ProdajnaMjestaFirme",
                newName: "GeografskaSirina");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "ProdajnaMjestaFirme",
                newName: "VlasnikId");

            migrationBuilder.RenameIndex(
                name: "IX_CompanySalePlaces_SalePlaceCategoryId",
                table: "ProdajnaMjestaFirme",
                newName: "IX_ProdajnaMjestaFirme_KategorijaProdajnogMjestaId");

            migrationBuilder.RenameIndex(
                name: "IX_CompanySalePlaces_CompanyId",
                table: "ProdajnaMjestaFirme",
                newName: "IX_ProdajnaMjestaFirme_VlasnikId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_KategorijaProdajnihMjesta",
                table: "KategorijaProdajnihMjesta",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipImanja",
                table: "TipImanja",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AktivnostiNadImanjem",
                table: "AktivnostiNadImanjem",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Imanja",
                table: "Imanja",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipFirme",
                table: "TipFirme",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProdajnaMjestaFirme",
                table: "ProdajnaMjestaFirme",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Firma_TipFirme_TipFirme",
                table: "Firma",
                column: "TipFirme",
                principalTable: "TipFirme",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Imanja_Firma_VlasnikId",
                table: "Imanja",
                column: "VlasnikId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Imanja_TipImanja_TipImanjaId",
                table: "Imanja",
                column: "TipImanjaId",
                principalTable: "TipImanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdajnaMjestaFirme_Firma_VlasnikId",
                table: "ProdajnaMjestaFirme",
                column: "VlasnikId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProdajnaMjestaFirme_KategorijaProdajnihMjesta_KategorijaProdajnogMjestaId",
                table: "ProdajnaMjestaFirme",
                column: "KategorijaProdajnogMjestaId",
                principalTable: "KategorijaProdajnihMjesta",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodPerProperties_Imanja_RuralPropertyId",
                table: "RuralGoodPerProperties",
                column: "RuralPropertyId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralOperations_Imanja_RuralPropertyId",
                table: "RuralOperations",
                column: "RuralPropertyId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_AktivnostiNadImanjem_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyActivityId",
                principalTable: "AktivnostiNadImanjem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_Imanja_RuralPropertyId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyId",
                principalTable: "Imanja",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Firma_TipFirme_TipFirme",
                table: "Firma");

            migrationBuilder.DropForeignKey(
                name: "FK_Imanja_Firma_VlasnikId",
                table: "Imanja");

            migrationBuilder.DropForeignKey(
                name: "FK_Imanja_TipImanja_TipImanjaId",
                table: "Imanja");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdajnaMjestaFirme_Firma_VlasnikId",
                table: "ProdajnaMjestaFirme");

            migrationBuilder.DropForeignKey(
                name: "FK_ProdajnaMjestaFirme_KategorijaProdajnihMjesta_KategorijaProdajnogMjestaId",
                table: "ProdajnaMjestaFirme");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralGoodPerProperties_Imanja_RuralPropertyId",
                table: "RuralGoodPerProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralOperations_Imanja_RuralPropertyId",
                table: "RuralOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_AktivnostiNadImanjem_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsPerRuralProperties_Imanja_RuralPropertyId",
                table: "ToolsPerRuralProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipImanja",
                table: "TipImanja");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipFirme",
                table: "TipFirme");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProdajnaMjestaFirme",
                table: "ProdajnaMjestaFirme");

            migrationBuilder.DropPrimaryKey(
                name: "PK_KategorijaProdajnihMjesta",
                table: "KategorijaProdajnihMjesta");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Imanja",
                table: "Imanja");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AktivnostiNadImanjem",
                table: "AktivnostiNadImanjem");

            migrationBuilder.RenameTable(
                name: "TipImanja",
                newName: "RuralPropertyTypes");

            migrationBuilder.RenameTable(
                name: "TipFirme",
                newName: "CompanyTypes");

            migrationBuilder.RenameTable(
                name: "ProdajnaMjestaFirme",
                newName: "CompanySalePlaces");

            migrationBuilder.RenameTable(
                name: "KategorijaProdajnihMjesta",
                newName: "SalePlaceCatogories");

            migrationBuilder.RenameTable(
                name: "Imanja",
                newName: "RuralProperties");

            migrationBuilder.RenameTable(
                name: "AktivnostiNadImanjem",
                newName: "RuralPropertyActivities");

            migrationBuilder.RenameColumn(
                name: "NazivTipaImanja",
                table: "RuralPropertyTypes",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralPropertyTypes",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "NazivTipaFirme",
                table: "CompanyTypes",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Naziv",
                table: "CompanySalePlaces",
                newName: "SalePlaceName");

            migrationBuilder.RenameColumn(
                name: "KategorijaProdajnogMjestaId",
                table: "CompanySalePlaces",
                newName: "SalePlaceCategoryId");

            migrationBuilder.RenameColumn(
                name: "GeografskaDuzina",
                table: "CompanySalePlaces",
                newName: "Longitude");

            migrationBuilder.RenameColumn(
                name: "GeografskaSirina",
                table: "CompanySalePlaces",
                newName: "Latitude");

            migrationBuilder.RenameColumn(
                name: "VlasnikId",
                table: "CompanySalePlaces",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdajnaMjestaFirme_KategorijaProdajnogMjestaId",
                table: "CompanySalePlaces",
                newName: "IX_CompanySalePlaces_SalePlaceCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_ProdajnaMjestaFirme_VlasnikId",
                table: "CompanySalePlaces",
                newName: "IX_CompanySalePlaces_CompanyId");

            migrationBuilder.RenameColumn(
                name: "NazivKategorijeProdajnogMjesta",
                table: "SalePlaceCatogories",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "MjernaJedinica",
                table: "RuralProperties",
                newName: "SizeMeasurementUnit");

            migrationBuilder.RenameColumn(
                name: "Velicina",
                table: "RuralProperties",
                newName: "Size");

            migrationBuilder.RenameColumn(
                name: "TipImanjaId",
                table: "RuralProperties",
                newName: "RuralPropertyTypeId");

            migrationBuilder.RenameColumn(
                name: "Naziv",
                table: "RuralProperties",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "GeografskaDuzina",
                table: "RuralProperties",
                newName: "Longitute");

            migrationBuilder.RenameColumn(
                name: "LokalniNaziv",
                table: "RuralProperties",
                newName: "LocalIdentifier");

            migrationBuilder.RenameColumn(
                name: "GeografskaSirina",
                table: "RuralProperties",
                newName: "Latitude");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralProperties",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "VlasnikId",
                table: "RuralProperties",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Imanja_TipImanjaId",
                table: "RuralProperties",
                newName: "IX_RuralProperties_RuralPropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Imanja_VlasnikId",
                table: "RuralProperties",
                newName: "IX_RuralProperties_CompanyId");

            migrationBuilder.RenameColumn(
                name: "Naziv",
                table: "RuralPropertyActivities",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "RuralPropertyActivities",
                newName: "Description");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralPropertyTypes",
                table: "RuralPropertyTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyTypes",
                table: "CompanyTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanySalePlaces",
                table: "CompanySalePlaces",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SalePlaceCatogories",
                table: "SalePlaceCatogories",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralProperties",
                table: "RuralProperties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RuralPropertyActivities",
                table: "RuralPropertyActivities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanySalePlaces_Firma_CompanyId",
                table: "CompanySalePlaces",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanySalePlaces_SalePlaceCatogories_SalePlaceCategoryId",
                table: "CompanySalePlaces",
                column: "SalePlaceCategoryId",
                principalTable: "SalePlaceCatogories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Firma_CompanyTypes_TipFirme",
                table: "Firma",
                column: "TipFirme",
                principalTable: "CompanyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralGoodPerProperties_RuralProperties_RuralPropertyId",
                table: "RuralGoodPerProperties",
                column: "RuralPropertyId",
                principalTable: "RuralProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralOperations_RuralProperties_RuralPropertyId",
                table: "RuralOperations",
                column: "RuralPropertyId",
                principalTable: "RuralProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralProperties_Firma_CompanyId",
                table: "RuralProperties",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralProperties_RuralPropertyTypes_RuralPropertyTypeId",
                table: "RuralProperties",
                column: "RuralPropertyTypeId",
                principalTable: "RuralPropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralPropertyActivities_RuralPropertyActivityId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyActivityId",
                principalTable: "RuralPropertyActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsPerRuralProperties_RuralProperties_RuralPropertyId",
                table: "ToolsPerRuralProperties",
                column: "RuralPropertyId",
                principalTable: "RuralProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
