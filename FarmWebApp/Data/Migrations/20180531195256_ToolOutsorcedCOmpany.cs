﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class ToolOutsorcedCOmpany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources");

            migrationBuilder.AlterColumn<int>(
                name: "ToolId",
                table: "ToolsOutsources",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "ToolsOutsources",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ToolsOutsources_CompanyId",
                table: "ToolsOutsources",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_Companies_CompanyId",
                table: "ToolsOutsources",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_Companies_CompanyId",
                table: "ToolsOutsources");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources");

            migrationBuilder.DropIndex(
                name: "IX_ToolsOutsources_CompanyId",
                table: "ToolsOutsources");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "ToolsOutsources");

            migrationBuilder.AlterColumn<int>(
                name: "ToolId",
                table: "ToolsOutsources",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
