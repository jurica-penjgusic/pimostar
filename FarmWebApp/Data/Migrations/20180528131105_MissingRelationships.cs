﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class MissingRelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RuralCategories");

            migrationBuilder.AddColumn<int>(
                name: "ToolId",
                table: "ToolsOutsources",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "ToolsAndMechanisms",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RuralPropertyId",
                table: "RuralOperations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ToolsOutsources_ToolId",
                table: "ToolsOutsources",
                column: "ToolId");

            migrationBuilder.CreateIndex(
                name: "IX_ToolsAndMechanisms_CompanyId",
                table: "ToolsAndMechanisms",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_RuralOperations_RuralPropertyId",
                table: "RuralOperations",
                column: "RuralPropertyId");

            migrationBuilder.AddForeignKey(
                name: "FK_RuralOperations_RuralProperties_RuralPropertyId",
                table: "RuralOperations",
                column: "RuralPropertyId",
                principalTable: "RuralProperties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsAndMechanisms_Companies_CompanyId",
                table: "ToolsAndMechanisms",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources",
                column: "ToolId",
                principalTable: "ToolsAndMechanisms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RuralOperations_RuralProperties_RuralPropertyId",
                table: "RuralOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsAndMechanisms_Companies_CompanyId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_ToolsAndMechanisms_ToolId",
                table: "ToolsOutsources");

            migrationBuilder.DropIndex(
                name: "IX_ToolsOutsources_ToolId",
                table: "ToolsOutsources");

            migrationBuilder.DropIndex(
                name: "IX_ToolsAndMechanisms_CompanyId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropIndex(
                name: "IX_RuralOperations_RuralPropertyId",
                table: "RuralOperations");

            migrationBuilder.DropColumn(
                name: "ToolId",
                table: "ToolsOutsources");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropColumn(
                name: "RuralPropertyId",
                table: "RuralOperations");

            migrationBuilder.CreateTable(
                name: "RuralCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuralCategories", x => x.Id);
                });
        }
    }
}
