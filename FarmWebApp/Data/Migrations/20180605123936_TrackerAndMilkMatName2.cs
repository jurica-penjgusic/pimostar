﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class TrackerAndMilkMatName2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TipPracenja",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(nullable: true),
                    Opis = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipPracenja", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Evidencija",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DatumZapisa = table.Column<DateTime>(nullable: false),
                    GospodarskoDobroId = table.Column<int>(nullable: true),
                    MjernaJedinica = table.Column<string>(nullable: true),
                    NazivEvidencije = table.Column<string>(nullable: true),
                    PrethodnoStanje = table.Column<decimal>(nullable: false),
                    TipPracenjaId = table.Column<int>(nullable: true),
                    TrenutnoStanje = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evidencija", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Evidencija_RuralGoods_GospodarskoDobroId",
                        column: x => x.GospodarskoDobroId,
                        principalTable: "RuralGoods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evidencija_TipPracenja_TipPracenjaId",
                        column: x => x.TipPracenjaId,
                        principalTable: "TipPracenja",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Evidencija_GospodarskoDobroId",
                table: "Evidencija",
                column: "GospodarskoDobroId");

            migrationBuilder.CreateIndex(
                name: "IX_Evidencija_TipPracenjaId",
                table: "Evidencija",
                column: "TipPracenjaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Evidencija");

            migrationBuilder.DropTable(
                name: "TipPracenja");
        }
    }
}
