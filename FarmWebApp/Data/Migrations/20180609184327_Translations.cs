﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FarmWebApp.Data.Migrations
{
    public partial class Translations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_CompanyTypes_CompanyTypeId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanySalePlaces_Companies_CompanyId",
                table: "CompanySalePlaces");

            migrationBuilder.DropForeignKey(
                name: "FK_MlijekoMati_Companies_CompanyId",
                table: "MlijekoMati");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralProperties_Companies_CompanyId",
                table: "RuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsAndMechanisms_Companies_CompanyId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_Companies_CompanyId",
                table: "ToolsOutsources");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Companies",
                table: "Companies");

            migrationBuilder.RenameTable(
                name: "Companies",
                newName: "Firma");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Firma",
                newName: "Naziv");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Firma",
                newName: "Opis");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "Firma",
                newName: "Osnovana");

            migrationBuilder.RenameColumn(
                name: "CompanyTypeId",
                table: "Firma",
                newName: "TipFirme");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_CompanyTypeId",
                table: "Firma",
                newName: "IX_Firma_TipFirme");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Firma",
                table: "Firma",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Firma_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanySalePlaces_Firma_CompanyId",
                table: "CompanySalePlaces",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Firma_CompanyTypes_TipFirme",
                table: "Firma",
                column: "TipFirme",
                principalTable: "CompanyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MlijekoMati_Firma_CompanyId",
                table: "MlijekoMati",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralProperties_Firma_CompanyId",
                table: "RuralProperties",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsAndMechanisms_Firma_CompanyId",
                table: "ToolsAndMechanisms",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_Firma_CompanyId",
                table: "ToolsOutsources",
                column: "CompanyId",
                principalTable: "Firma",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Firma_CompanyId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanySalePlaces_Firma_CompanyId",
                table: "CompanySalePlaces");

            migrationBuilder.DropForeignKey(
                name: "FK_Firma_CompanyTypes_TipFirme",
                table: "Firma");

            migrationBuilder.DropForeignKey(
                name: "FK_MlijekoMati_Firma_CompanyId",
                table: "MlijekoMati");

            migrationBuilder.DropForeignKey(
                name: "FK_RuralProperties_Firma_CompanyId",
                table: "RuralProperties");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsAndMechanisms_Firma_CompanyId",
                table: "ToolsAndMechanisms");

            migrationBuilder.DropForeignKey(
                name: "FK_ToolsOutsources_Firma_CompanyId",
                table: "ToolsOutsources");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Firma",
                table: "Firma");

            migrationBuilder.RenameTable(
                name: "Firma",
                newName: "Companies");

            migrationBuilder.RenameColumn(
                name: "Naziv",
                table: "Companies",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Opis",
                table: "Companies",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "Osnovana",
                table: "Companies",
                newName: "Created");

            migrationBuilder.RenameColumn(
                name: "TipFirme",
                table: "Companies",
                newName: "CompanyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Firma_TipFirme",
                table: "Companies",
                newName: "IX_Companies_CompanyTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Companies",
                table: "Companies",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Companies_CompanyId",
                table: "AspNetUsers",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_CompanyTypes_CompanyTypeId",
                table: "Companies",
                column: "CompanyTypeId",
                principalTable: "CompanyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanySalePlaces_Companies_CompanyId",
                table: "CompanySalePlaces",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MlijekoMati_Companies_CompanyId",
                table: "MlijekoMati",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RuralProperties_Companies_CompanyId",
                table: "RuralProperties",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsAndMechanisms_Companies_CompanyId",
                table: "ToolsAndMechanisms",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ToolsOutsources_Companies_CompanyId",
                table: "ToolsOutsources",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
