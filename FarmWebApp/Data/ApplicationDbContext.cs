﻿using FarmWebApp.Models;
using FarmWebApp.Models.Company;
using FarmWebApp.Models.Evidencija;
using FarmWebApp.Models.MlijekoMati;
using FarmWebApp.Models.RuralGoodGathering;
using FarmWebApp.Models.RuralGoods;
using FarmWebApp.Models.RuralOperations;
using FarmWebApp.Models.RuralProperties;
using FarmWebApp.Models.SalePlaces;
using FarmWebApp.Models.Season;
using FarmWebApp.Models.ToolsAndMechanism;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FarmWebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Company> Companies { get; set; }

        public DbSet<CompanyType> CompanyTypes { get; set; }

        public DbSet<RuralProperty> RuralProperties { get; set; }

        public DbSet<RuralPropertyType> RuralPropertyTypes { get; set; }

        public DbSet<CompanySalePlace> CompanySalePlaces { get; set; }

        public DbSet<SalePlaceCatogory> SalePlaceCatogories { get; set; }

        public DbSet<RuralPropertyActivity> RuralPropertyActivities { get; set; }

        public DbSet<ToolCost> ToolCosts { get; set; }

        public DbSet<ToolCost> ToolCostTypes { get; set; }

        public DbSet<ToolsAndMechanism> ToolsAndMechanisms { get; set; }

        public DbSet<ToolsOutsourced> ToolsOutsources { get; set; }

        public DbSet<ToolsPerRuralProperty> ToolsPerRuralProperties { get; set; }

        public DbSet<RuralGood> RuralGoods { get; set; }

        public DbSet<RuralCostType> RuralCostTypes { get; set; }

        public DbSet<RuralGoodCategory> RuralGoodCategories { get; set; }

        public DbSet<RuralGoodGathering> RuralGoodGatherings { get; set; }

        public DbSet<GatheringCosts> GatheringCosts { get; set; }

        public DbSet<RuralGoodCosts> RuralGoodCosts { get; set; }

        public DbSet<RuralGoodPerProperty> RuralGoodPerProperties { get; set; }

        public DbSet<OperationActivityType> OperationActivityTypes { get; set; }

        public DbSet<OperationType> OperationTypes { get; set; }

        public DbSet<RuralOperations> RuralOperations { get; set; }

        public DbSet<Season> Seasons { get; set; }

        public DbSet<MlijekoMat> MlijekoMati { get; set; }

        public DbSet<Trosak> Trosaks { get; set; }

        public DbSet<ToolCostType> ToolCostType { get; set; }


        public DbSet<Trosak> Trosak { get; set; }


        public DbSet<TroskoviMlijekomata> TroskoviMlijekomata { get; set; }


        public DbSet<TipRadnje> TipRadnje { get; set; }


        public DbSet<PrihodiMlijekomata> PrihodiMlijekomata { get; set; }


        public DbSet<TipPracenja> TipPracenja { get; set; }


        public DbSet<Evidencija> Evidencija { get; set; }


        public DbSet<TipAktivnosti> TipAktivnosti { get; set; }


        public DbSet<EvidencijaORadnjama> EvidencijaORadnjama { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Company Translation
            builder.Entity<Company>().ToTable("Firma");
            builder.Entity<Company>().Property(x => x.Name).HasColumnName("Naziv");
            builder.Entity<Company>().Property(x => x.CompanyTypeId).HasColumnName("TipFirme");
            builder.Entity<Company>().Property(x => x.Created).HasColumnName("Osnovana");
            builder.Entity<Company>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<CompanyType>().ToTable("TipFirme");
            builder.Entity<CompanyType>().Property(x => x.Name).HasColumnName("NazivTipaFirme");

            builder.Entity<RuralProperty>().ToTable("Imanja");
            builder.Entity<RuralProperty>().Property(x => x.CompanyId).HasColumnName("VlasnikId");
            builder.Entity<RuralProperty>().Property(x => x.Name).HasColumnName("Naziv");
            builder.Entity<RuralProperty>().Property(x => x.Description).HasColumnName("Opis");
            builder.Entity<RuralProperty>().Property(x => x.Latitude).HasColumnName("GeografskaSirina");
            builder.Entity<RuralProperty>().Property(x => x.Longitute).HasColumnName("GeografskaDuzina");
            builder.Entity<RuralProperty>().Property(x => x.LocalIdentifier).HasColumnName("LokalniNaziv");
            builder.Entity<RuralProperty>().Property(x => x.RuralPropertyTypeId).HasColumnName("TipImanjaId");
            builder.Entity<RuralProperty>().Property(x => x.Size).HasColumnName("Velicina");
            builder.Entity<RuralProperty>().Property(x => x.SizeMeasurementUnit).HasColumnName("MjernaJedinica");


            builder.Entity<RuralPropertyType>().ToTable("TipImanja");
            builder.Entity<RuralPropertyType>().Property(x => x.Name).HasColumnName("NazivTipaImanja");
            builder.Entity<RuralPropertyType>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<CompanySalePlace>().ToTable("ProdajnaMjestaFirme");
            builder.Entity<CompanySalePlace>().Property(x => x.CompanyId).HasColumnName("VlasnikId");
            builder.Entity<CompanySalePlace>().Property(x => x.Latitude).HasColumnName("GeografskaSirina");
            builder.Entity<CompanySalePlace>().Property(x => x.Longitude).HasColumnName("GeografskaDuzina");
            builder.Entity<CompanySalePlace>().Property(x => x.SalePlaceCategoryId)
                .HasColumnName("KategorijaProdajnogMjestaId");
            builder.Entity<CompanySalePlace>().Property(x => x.SalePlaceName).HasColumnName("Naziv");

            builder.Entity<SalePlaceCatogory>().ToTable("KategorijaProdajnihMjesta");
            builder.Entity<SalePlaceCatogory>().Property(x => x.Name).HasColumnName("NazivKategorijeProdajnogMjesta");

            builder.Entity<RuralPropertyActivity>().ToTable("AktivnostiNadImanjem");
            builder.Entity<RuralPropertyActivity>().Property(x => x.Name).HasColumnName("Naziv");
            builder.Entity<RuralPropertyActivity>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<ToolCost>().ToTable("TroskoviAlataIMehanizacije");
            builder.Entity<ToolCost>().Property(x => x.Name).HasColumnName("NazivTroska");
            builder.Entity<ToolCost>().Property(x => x.Amount).HasColumnName("Iznos");
            builder.Entity<ToolCost>().Property(x => x.CostTime).HasColumnName("DatumTroska");
            builder.Entity<ToolCost>().Property(x => x.CostTypeId).HasColumnName("TipTroskaId");
            builder.Entity<ToolCost>().Property(x => x.ToolsAndMechanismId).HasColumnName("AlatIMehanizacijaId");
            builder.Entity<ToolCost>().Property(x => x.Currency).HasColumnName("ValutaPlacanja");

            builder.Entity<ToolsAndMechanism>().ToTable("AlatiIMehanizacija");
            builder.Entity<ToolsAndMechanism>().Property(x => x.CompanyId).HasColumnName("VlasnikId");
            builder.Entity<ToolsAndMechanism>().Property(x => x.Amount).HasColumnName("KolicinaNaSkladistu");
            builder.Entity<ToolsAndMechanism>().Property(x => x.Description).HasColumnName("OpisAlata");
            builder.Entity<ToolsAndMechanism>().Property(x => x.Name).HasColumnName("NazivAlata");
            builder.Entity<ToolsAndMechanism>().Property(x => x.UnitMeasurement)
                .HasColumnName("MjernaJedinicaKolicineNaSkladistu");

            builder.Entity<ToolsOutsourced>().ToTable("NajamAlataIMehanizacije");
            builder.Entity<ToolsOutsourced>().Property(x => x.CompanyId).HasColumnName("VlasnikId");
            builder.Entity<ToolsOutsourced>().Property(x => x.Currency).HasColumnName("ValutaPlacanja");
            builder.Entity<ToolsOutsourced>().Property(x => x.Description).HasColumnName("Opis");
            builder.Entity<ToolsOutsourced>().Property(x => x.From).HasColumnName("PocetakNajma");
            builder.Entity<ToolsOutsourced>().Property(x => x.To).HasColumnName("KrajNajma");
            builder.Entity<ToolsOutsourced>().Property(x => x.Price).HasColumnName("Cijena");
            builder.Entity<ToolsOutsourced>().Property(x => x.ToolId).HasColumnName("AlatIMehanizacijaId");

            builder.Entity<ToolsPerRuralProperty>().ToTable("AlatiIMehanizacijaPoImanju");
            builder.Entity<ToolsPerRuralProperty>().Property(x => x.RuralPropertyId).HasColumnName("ImanjeId");
            builder.Entity<ToolsPerRuralProperty>().Property(x => x.From).HasColumnName("VrijemePocetka");
            builder.Entity<ToolsPerRuralProperty>().Property(x => x.To).HasColumnName("VrijemeKraja");
            builder.Entity<ToolsPerRuralProperty>().Property(x => x.ToolId).HasColumnName("AlatIMehanizacijaId");
            builder.Entity<ToolsPerRuralProperty>().Property(x => x.RuralPropertyActivityId)
                .HasColumnName("AktivnostId");

            builder.Entity<RuralGood>().ToTable("GospodarskoDobro");
            builder.Entity<RuralGood>().Property(x => x.Name).HasColumnName("NazivGosporaskogDobra");
            builder.Entity<RuralGood>().Property(x => x.Description).HasColumnName("Opis");
            builder.Entity<RuralGood>().Property(x => x.Active).HasColumnName("Zivo");
            builder.Entity<RuralGood>().Property(x => x.RuralCategoryId).HasColumnName("KategorijaDobraId");

            builder.Entity<RuralCostType>().ToTable("TipTroskovaDobara");
            builder.Entity<RuralCostType>().Property(x => x.Name).HasColumnName("NazivTipaTroska");
            builder.Entity<RuralCostType>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<RuralGoodCategory>().ToTable("KategorijaDobra");
            builder.Entity<RuralGoodCategory>().Property(x => x.Name).HasColumnName("NazivKategorije");
            builder.Entity<RuralGoodCategory>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<RuralGoodGathering>().ToTable("Berba");
            builder.Entity<RuralGoodGathering>().Property(x => x.Amount).HasColumnName("Kolicina");
            builder.Entity<RuralGoodGathering>().Property(x => x.GatheringCostId).HasColumnName("TrosakId");
            builder.Entity<RuralGoodGathering>().Property(x => x.GatheringTime).HasColumnName("DatumeBerbe");
            builder.Entity<RuralGoodGathering>().Property(x => x.MeasurementUnit).HasColumnName("MjernaJedinica");
            builder.Entity<RuralGoodGathering>().Property(x => x.RuralGoodPerPropertyId).HasColumnName("ImanjeId");
            builder.Entity<RuralGoodGathering>().Property(x => x.SeasonId).HasColumnName("SezonaId");

            builder.Entity<GatheringCosts>().ToTable("TroskoviBerbe");
            builder.Entity<GatheringCosts>().Property(x => x.Name).HasColumnName("NazivTroska");
            builder.Entity<GatheringCosts>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<RuralGoodCosts>().ToTable("TroskoviDobara");
            builder.Entity<RuralGoodCosts>().Property(x => x.Name).HasColumnName("NazivTroska");
            builder.Entity<RuralGoodCosts>().Property(x => x.Amount).HasColumnName("Iznos");
            builder.Entity<RuralGoodCosts>().Property(x => x.Created).HasColumnName("DatumTroska");
            builder.Entity<RuralGoodCosts>().Property(x => x.Currency).HasColumnName("Valuta");
            builder.Entity<RuralGoodCosts>().Property(x => x.RuralCostTypeId).HasColumnName("TipTroskaId");

            builder.Entity<RuralGoodPerProperty>().ToTable("GospodarskaDobraPoImanjima");
            builder.Entity<RuralGoodPerProperty>().Property(x => x.MeasurementUnit).HasColumnName("MjernaJedinica");
            builder.Entity<RuralGoodPerProperty>().Property(x => x.Amount).HasColumnName("Kolicina");
            builder.Entity<RuralGoodPerProperty>().Property(x => x.RuralGoodId).HasColumnName("GospodarskoDobroId");
            builder.Entity<RuralGoodPerProperty>().Property(x => x.RuralPropertyId).HasColumnName("ImanjeId");
            builder.Entity<RuralGoodPerProperty>().Property(x => x.TimeOfProductPerLand).HasColumnName("VremenskiPeriod");

            builder.Entity<OperationActivityType>().ToTable("TipoviAktivnosti");
            builder.Entity<OperationActivityType>().Property(x => x.Name).HasColumnName("NazivTipaAktivnosti");
            builder.Entity<OperationActivityType>().Property(x => x.Description).HasColumnName("Opis");

            builder.Entity<OperationType>().ToTable("TipRadnji");
            builder.Entity<OperationType>().Property(x => x.Name).HasColumnName("NazivRadnje");
            builder.Entity<OperationType>().Property(x => x.Description).HasColumnName("Opis");
            builder.Entity<OperationType>().Property(x => x.SeasonId).HasColumnName("SezonaId");

            builder.Entity<RuralOperations>().ToTable("RadnjeNadImanjima");
            builder.Entity<RuralOperations>().Property(x => x.Name).HasColumnName("NazivRadnje");
            builder.Entity<RuralOperations>().Property(x => x.RuralPropertyId).HasColumnName("ImanjeId");
            builder.Entity<RuralOperations>().Property(x => x.Cost).HasColumnName("UkupanTrosak");
            builder.Entity<RuralOperations>().Property(x => x.Currency).HasColumnName("Valuta");
            builder.Entity<RuralOperations>().Property(x => x.Description).HasColumnName("Opis");
            builder.Entity<RuralOperations>().Property(x => x.MeasurementUnit).HasColumnName("MjernaJedinica");
            builder.Entity<RuralOperations>().Property(x => x.OperationActivityTypeId).HasColumnName("AktivnostId");
            builder.Entity<RuralOperations>().Property(x => x.OperationDateTime).HasColumnName("DatumRadnje");
            builder.Entity<RuralOperations>().Property(x => x.OperationTypeId).HasColumnName("TipRadnjeId");

            builder.Entity<ToolCostType>().ToTable("TipTroskovaAlataIMehanizacije");
            builder.Entity<ToolCostType>().Property(x => x.Name).HasColumnName("NazivTipaTroska");
        }
    }
}